import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {

    @Test
    public void testSum() {
        // prepare
        Calculator calculator = new Calculator();
        int expected = 5;

        // when
        int actual = calculator.sum(2, 3);

        // then
        assertEquals(expected, actual);
    }

    @Test
    public void testDivide() {
        // prepare
        Calculator calculator = new Calculator();
        int expected = 5;

        // when
        int actual = calculator.divide(15, 3);

        // then
        assertEquals(expected, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDivideByZero() {
        // prepare
        Calculator calculator = new Calculator();

        // when
        calculator.divide(15, 0);
        // check that IllegalArgument thrown
    }
}
