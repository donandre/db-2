import org.junit.Ignore;
import org.junit.Test;
import com.collections.*;

import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;

public class ArrayQueueTest {
    @Test
    public void testEnqueueAndDequeue() {
        ArrayQueue arrayQueue = new ArrayQueue();
        arrayQueue.enqueue("A");
        arrayQueue.enqueue("B");
        arrayQueue.enqueue("C");

        assertEquals(3, arrayQueue.size());

        assertEquals("A", arrayQueue.dequeue());
        assertEquals("B", arrayQueue.dequeue());
        arrayQueue.enqueue("D");

        assertEquals(2, arrayQueue.size());
        assertEquals("C", arrayQueue.dequeue());
        assertEquals("D", arrayQueue.dequeue());
        assertEquals(0, arrayQueue.size());
    }

    @Test(expected = NoSuchElementException.class)
    public void testPeekOnEmtpyQueue (){
        ArrayQueue arrayQueue = new ArrayQueue();
        arrayQueue.peek();
    }

    @Ignore
    @Test
    public void testEnqueueAndDequeueWithIncreasedArray() {
        ArrayQueue arrayQueue = new ArrayQueue();
        for (char c = 'A'; c != 'Z' + 1; c++) {
            String value = String.valueOf(c);
            arrayQueue.enqueue(value);
        }

        int expectedCount = 'Z' - 'A' + 1;
        assertEquals(expectedCount, arrayQueue.size());

        for (char c = 'A'; c != 'Z' + 1; c++) {
            String value = String.valueOf(c);
            assertEquals(value, arrayQueue.dequeue());
        }
    }

    @Test
    public void testEnqueueAndDequeueWithIncreasedArray2() {
        ArrayQueue arrayQueue = new ArrayQueue();
        arrayQueue.enqueue("A");
        arrayQueue.enqueue("A1");
        arrayQueue.enqueue("A2");
        arrayQueue.enqueue("A3");
        arrayQueue.enqueue("A4");
        arrayQueue.enqueue("A5");


        assertEquals(6, arrayQueue.size());

        assertEquals("A", arrayQueue.dequeue());
        assertEquals("A1", arrayQueue.dequeue());
        assertEquals("A2", arrayQueue.dequeue());
        assertEquals("A3", arrayQueue.dequeue());
        assertEquals("A4", arrayQueue.dequeue());
        assertEquals("A5", arrayQueue.dequeue());

        assertEquals(0, arrayQueue.size());
    }

}