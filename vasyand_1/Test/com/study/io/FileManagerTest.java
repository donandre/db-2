package com.study.io;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;

import static junit.framework.TestCase.assertEquals;

public class FileManagerTest {
    @Before
    public void before() throws IOException {
        File rootDir = new File("root");
        rootDir.mkdir();
        File rootFile = new File(rootDir.getAbsolutePath() + "/root.log");
        rootFile.createNewFile();
        File firstDir = new File(rootDir.getAbsolutePath() + "/firstLevel/");
        firstDir.mkdir();
        File firstLevel1File = new File(rootDir.getAbsolutePath() + "/firstLevel/firstLevel1.log");
        firstLevel1File.createNewFile();
        File firstLevel2File = new File(rootDir.getAbsolutePath() + "/firstLevel/firstLevel2.log");
        firstLevel2File.createNewFile();
        File secondDir = new File(rootDir.getAbsolutePath() + "/secondLevel/");
        secondDir.mkdir();
        File secondLevel1File = new File(rootDir.getAbsolutePath() + "/secondLevel/secondLevel1.log");
        secondLevel1File.createNewFile();
        File secondLevel2File = new File(rootDir.getAbsolutePath() + "/secondLevel/secondLevel2.log");
        secondLevel2File.createNewFile();
        File secondLevelDir = new File(rootDir.getAbsolutePath() + "/secondLevel/secondLevelChild");
        secondLevelDir.mkdir();
    }

    @Test
    public void countFiles() {
        assertEquals(5, FileManager.countFiles("root"));
    }

    @Test
    public void countDirs() {
        assertEquals(3, FileManager.countDirs("root"));
    }

    @Test
    public void copyDir() {
        FileManager.copyDir("root", "root_copy");
        assertEquals(3, FileManager.countDirs("root"));
        assertEquals(5, FileManager.countFiles("root"));
        assertEquals(3, FileManager.countDirs("root_copy"));
        assertEquals(5, FileManager.countFiles("root_copy"));
    }

    @Test
    public void moveDir(){
        FileManager.moveDir("root", "root_move");
        assertEquals(0, FileManager.countDirs("root"));
        assertEquals(0, FileManager.countFiles("root"));
        assertEquals(3, FileManager.countDirs("root_move"));
        assertEquals(5, FileManager.countFiles("root_move"));

    }

    @After
    public void After() {
        ArrayList<File> allPaths = new ArrayList<File>();
        allPaths.add(new File("root"));
        allPaths.add(new File("root_copy"));
        for (File rootDir : allPaths) {
            if (rootDir.exists()) {
                File secondLevelDir = new File(rootDir.getAbsolutePath() + "/secondLevel/secondLevelChild");
                secondLevelDir.delete();
                File secondLevel2File = new File(rootDir.getAbsolutePath() + "/secondLevel/secondLevel2.log");
                secondLevel2File.delete();
                File secondLevel1File = new File(rootDir.getAbsolutePath() + "/secondLevel/secondLevel1.log");
                secondLevel1File.delete();
                File secondDir = new File(rootDir.getAbsolutePath() + "/secondLevel/");
                secondDir.delete();
                File firstLevel2File = new File(rootDir.getAbsolutePath() + "/firstLevel/firstLevel2.log");
                firstLevel2File.delete();
                File firstLevel1File = new File(rootDir.getAbsolutePath() + "/firstLevel/firstLevel1.log");
                firstLevel1File.delete();
                File firstDir = new File(rootDir.getAbsolutePath() + "/firstLevel/");
                firstDir.delete();
                File rootFile = new File(rootDir.getAbsolutePath() + "/root.log");
                rootFile.delete();
                rootDir.delete();
            }
        }

    }

}