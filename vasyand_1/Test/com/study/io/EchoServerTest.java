package com.study.io;

import com.sun.xml.internal.messaging.saaj.util.ByteInputStream;
import org.junit.Test;

import java.io.*;

import static org.junit.Assert.*;

public class EchoServerTest {

    @Test
    public void doServerProcessing() throws IOException {
        String input = "test_input";
        OutputStream result = new ByteArrayOutputStream();
        EchoServer.doServerProcessing(new ByteArrayInputStream(input.getBytes()), result);
        assertEquals("Echo "+input, result.toString());
    }
}