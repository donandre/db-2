package com.study.io;


import com.study.io.WebServer.WebServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.List;

import static org.junit.Assert.*;

public class WebServerTest {
    @Before
    public void before() throws IOException {

    }
/*
    @Test
    public void getFileContent() throws IOException {
        File rootDir = new File("");
        String str = rootDir.getAbsolutePath();
        File rootFile = new File(rootDir.getAbsolutePath() + "\\resources\\webapp\\tst.log");
        rootFile.createNewFile();
        List<String> content = new java.util.ArrayList<>();
        content.add("One");
        content.add("Two");
        content.add("next line will be breaked");
        content.add("");
        content.add("last line");
        FileWriter fw = new FileWriter(rootDir.getAbsolutePath() + "\\resources\\webapp\\tst.log");
        for (String line:content) {
            fw.write(line);
            fw.write("\n");
        }
        fw.close();
        WebServer ws = new WebServer();
        //List<String> actualResult = ws.getFileContent(rootFile);
        //assertTrue(content.containsAll(actualResult));
        //assertTrue(actualResult.containsAll(content));
        rootFile.delete();
    }

    @Test
    public void writeMessageToOutputStream() throws IOException {

        List<String> content = new java.util.ArrayList<>();
        content.add("One");
        content.add("Two");
        content.add("next line will be breaked");
        content.add("");
        content.add("last line");
        ByteArrayOutputStream expectedOutput = new ByteArrayOutputStream();
        for (String line : content) {
            expectedOutput.write(line.getBytes());
            expectedOutput.write("\n".getBytes());
        }
        WebServer ws = new WebServer();
        OutputStream actualResultStream = new ByteArrayOutputStream();
        ws.writeMessageToOutputStream(content, actualResultStream);
        ByteArrayOutputStream actualResultOutputByteArray = (ByteArrayOutputStream) actualResultStream;
       assertArrayEquals(expectedOutput.toByteArray(), actualResultOutputByteArray.toByteArray());

    }

    @Test
    public void getMessageFromInputStream() throws IOException {
        List<String> content = new java.util.ArrayList<>();
        content.add("One");
        content.add("Two");
        content.add("next line will be breaked");
        content.add("");
        content.add("last line");
        ByteArrayOutputStream bOutS = new ByteArrayOutputStream();
        for (String line : content) {
            bOutS.write(line.getBytes());
            bOutS.write("\n".getBytes());
        }
        byte[] bytes = bOutS.toByteArray();
        InputStream actualIS = new ByteArrayInputStream(bytes);
        WebServer ws = new WebServer();
        List<String> actualResult = ws.getMessageFromInputStream(actualIS);
        assertTrue(content.containsAll(actualResult));
        assertTrue(actualResult.containsAll(content));
    }

    @Test
    public void getRequestedFilePath() throws IOException {
        String expected = "/hello.htm";
        List<String> content = new java.util.ArrayList<>();
        content.add("GET "+expected+" HTTP/1.1");
        content.add("Two sdf");
        content.add("sdfffdsdf");
        content.add("");
        content.add("");


        ByteArrayOutputStream bOutS = new ByteArrayOutputStream();
        for (String line : content) {
            bOutS.write(line.getBytes());
            bOutS.write("\n".getBytes());
        }
        byte[] bytes = bOutS.toByteArray();
        InputStream actualIS = new ByteArrayInputStream(bytes);
        WebServer ws = new WebServer();
        String actualResult = ws.getRequestedFilePath(actualIS);
        assertEquals(expected, actualResult);
    }

    @Test
    public void doServerProcessing() {

    }

    @Test
    public void setPort() {
    }

    @Test
    public void setWebAppPath() {
    }

    @Test
    public void start() {
    }

    @Test
    public void canBeStarted() {
    }

    @Test
    public void stop() {
    }

    @Test
    public void isStarted() {
    }

    @After
    public void after() {
    }
    */
}