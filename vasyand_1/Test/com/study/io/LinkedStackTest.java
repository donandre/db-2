package com.study.io;

import org.junit.Test;
import com.collections.*;
import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class LinkedStackTest {
    @Test
    public void TestPushAndPop(){
        LinkedStack linkedStack = new LinkedStack();
        linkedStack.push("A");
        linkedStack.push("B");
        linkedStack.push("C");
        assertEquals(3, linkedStack.size());
        assertEquals("C",linkedStack.pop());
        assertEquals(2, linkedStack.size());
        assertEquals("B",linkedStack.pop());
        assertEquals(1, linkedStack.size());
        assertEquals("A",linkedStack.pop());
        assertEquals(0, linkedStack.size());
    }
    @Test
    public void TestPeek(){
        LinkedStack linkedStack = new LinkedStack();
        linkedStack.push("A");
        linkedStack.push("B");
        assertEquals(2, linkedStack.size());
        assertEquals("B",linkedStack.peek());
        assertEquals(2, linkedStack.size());

    }
    @Test
    public void TestContains(){
        LinkedStack linkedStack = new LinkedStack();
        linkedStack.push("A");
        linkedStack.push("B");
        linkedStack.push("C");
        assertTrue(linkedStack.equals("A"));
    }
}
