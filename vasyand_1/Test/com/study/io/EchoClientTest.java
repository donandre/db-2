package com.study.io;


import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import static org.junit.Assert.*;

public class EchoClientTest {
    @Before
    public void before() {
        //starting test server in background
        new Thread(new Runnable() {
            public void run() {
                EchoServer testServer = new EchoServer();
            }
        }).start();
    }

    @Test
    public void openConnection() {
        EchoClient testClient = new EchoClient();
        assertNull(testClient.getSocket());
        try {
            testClient.openConnection("localhost", 123);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertTrue(testClient.getSocket().isConnected());
    }

    @Test
    public void closeConnectionIfConnected() throws IOException {
        EchoClient testClient = new EchoClient();
        assertNull(testClient.getSocket());
        try {
            testClient.openConnection("localhost", 123);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertTrue(testClient.getSocket().isConnected());
        testClient.closeConnectionIfConnected();
        assertNull(testClient.getSocket());

    }

    @Test
    public void writeMessageToOutputStream() throws IOException {
        EchoClient testClient = new EchoClient();
        String testInput = "Test String";
        List<String> message = new ArrayList<>();
        message.add(testInput);
        testClient.output = new ByteArrayOutputStream(0);
        testClient.writeMessageToOutputStream(message);


    }

    @Test
    public void getMessageFromInputStream() throws IOException {
        EchoClient testClient = new EchoClient();
        String testInput = "Test String";
        testClient.input = new ByteArrayInputStream(testInput.getBytes());
        assertEquals(testInput, testClient.getMessageFromInputStream());
    }
}
