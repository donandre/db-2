package com.study.io;

import com.collections.HashMap;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;
import java.util.NoSuchElementException;

import static org.junit.Assert.*;

public class HashMapTest {
    private static HashMap testHashMap;


    @Test
    public void put() {
        testHashMap = new HashMap();
        testHashMap.putIfAbsent("1", "One");
        assertEquals("One", testHashMap.put("1", "One_new"));
        //  assertEquals(new NoSuchElementException().getClass().getName(), testHashMap.put("2","two").getClass().getName());
        assertEquals("One_new", testHashMap.get("1"));
    }

    @Test(expected = NoSuchElementException.class)
    public void put$noSuchElement() {
        testHashMap = new HashMap();
        testHashMap.putIfAbsent("1", "One");
        testHashMap.put("2", "two");
    }

    @Test(expected = NullPointerException.class)
    public void put$nullPointer() {
        testHashMap = new HashMap();
        testHashMap.putIfAbsent("1", "One");
        testHashMap.put(null, "Two");
    }

    @Test
    public void putIfAbsent() {
        testHashMap = new HashMap();
        testHashMap.putIfAbsent("1", "One");
        assertEquals(1, testHashMap.size());
        assertEquals("One", testHashMap.get("1"));
        testHashMap.putIfAbsent("2", "Two");
        assertEquals(2, testHashMap.size());
        assertEquals("Two", testHashMap.get("2"));
        testHashMap.putIfAbsent("2", "Two_new");
        assertEquals(2, testHashMap.size());
        assertEquals("Two_new", testHashMap.get("2"));
    }

    @Test(expected = NullPointerException.class)
    public void putIfAbsent$nullPointer() {
        testHashMap = new HashMap();
        testHashMap.putIfAbsent("1", "One");
        testHashMap.putIfAbsent(null, "Two");
    }

    @Test
    public void get() {
        testHashMap = new HashMap();
        testHashMap.putIfAbsent("1", "One");
        assertEquals("One", testHashMap.get("1"));
        assertEquals(1, testHashMap.size());
        testHashMap.putIfAbsent("2", "Two");
        assertEquals("Two", testHashMap.get("2"));
        assertEquals(2, testHashMap.size());
    }

    @Test(expected = NoSuchElementException.class)
    public void get$noSuchElement() {
        testHashMap = new HashMap();
        testHashMap.putIfAbsent("1", "One");
        testHashMap.get("2");
    }

    @Test(expected = NullPointerException.class)
    public void get$nullPointer() {
        testHashMap = new HashMap();
        testHashMap.putIfAbsent("1", "One");
        testHashMap.get(null);
    }

    @Test
    public void remove() {
        testHashMap = new HashMap();
        testHashMap.putIfAbsent("1", "One");
        assertEquals(1, testHashMap.size());
        assertEquals("One", testHashMap.remove("1"));
        assertEquals(0, testHashMap.size());
    }

    @Test(expected = NoSuchElementException.class)
    public void remove$noSuchElement() {
        testHashMap = new HashMap();
        testHashMap.putIfAbsent("1", "One");
        testHashMap.remove("2");
    }

    @Test(expected = NullPointerException.class)
    public void remove$nullPointer() {
        testHashMap = new HashMap();
        testHashMap.putIfAbsent("1", "One");
        testHashMap.remove(null);
    }

    @Test
    public void size() {
        testHashMap = new HashMap();
        assertEquals(0, testHashMap.size());
        testHashMap.putIfAbsent("1", "One");
        assertEquals(1, testHashMap.size());
        testHashMap.putIfAbsent("2", "Two");
        assertEquals(2, testHashMap.size());
        testHashMap.putIfAbsent("3", "Three");
        assertEquals(3, testHashMap.size());
    }

    @Test
    public void containsKey() {
        testHashMap = new HashMap();
        testHashMap.putIfAbsent("1", "One");
        assertTrue(testHashMap.containsKey("1"));
        assertFalse(testHashMap.containsKey("2"));
    }

    @Test(expected = NullPointerException.class)
    public void containsKey$NullPointer() {
        testHashMap = new HashMap();
        testHashMap.putIfAbsent("1", "One");
        testHashMap.containsKey(null);
    }


    @Test
    public void iterator() {
        testHashMap = new HashMap();
        Iterator hashMapIterator = testHashMap.iterator();
        assertFalse(hashMapIterator.hasNext());
        testHashMap.putIfAbsent("1", "One");
        testHashMap.putIfAbsent("2", "Two");
        testHashMap.putIfAbsent("3", "Three");
        hashMapIterator = testHashMap.iterator();
        assertTrue(hashMapIterator.hasNext());
        int elementsCount = testHashMap.size();
        for (int i = testHashMap.size(); i > 0; i--) {
            hashMapIterator.next();
        }
        assertFalse(hashMapIterator.hasNext());
    }
}