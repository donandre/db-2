package com.annotation;

import org.junit.Test;

import static org.junit.Assert.*;

public class QueryGeneratorTest {

    @Test
    public void getAll() {
        QueryGenerator queryGenerator = new QueryGenerator();
        String expectedSql = "SELECT person_id, person_name, salary FROM Persons;";
        String actualSql = queryGenerator.getAll(Person.class);
        assertEquals(expectedSql, actualSql);

    }

    @Test
    public void insert() {
        Person newPerson = new Person();
        newPerson.id=5;
        newPerson.name="John";
        newPerson.salary=4.00d;
        QueryGenerator queryGenerator = new QueryGenerator();
        String expectedSql = "INSERT INTO Persons (person_id, person_name, salary) VALUES (5, 'John', 4);";
        String actualSql = queryGenerator.insert(newPerson);
        assertEquals(expectedSql, actualSql);
    }

    @Test
    public void getById() {
        int testId = 5;
        QueryGenerator queryGenerator = new QueryGenerator();
        String expectedSql = "SELECT person_id, person_name, salary FROM Persons WHERE person_id = 5;";
        String actualSql = queryGenerator.getById(Person.class, testId);
        assertEquals(expectedSql, actualSql);
    }

    @Test
    public void delete() {
        int testId = 6;
        QueryGenerator queryGenerator = new QueryGenerator();
        String expectedSql = "DELETE FROM Persons WHERE person_id = 6;";
        String actualSql = queryGenerator.delete(Person.class, testId);
        assertEquals(expectedSql, actualSql);

    }
}