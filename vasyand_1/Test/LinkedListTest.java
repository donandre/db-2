import org.junit.Test;
import static org.junit.Assert.*;
import com.collections.*;
public class LinkedListTest {
    @Test
    public void testAdd(){
        LinkedList linkedList = new LinkedList();
        linkedList.add("A");
        linkedList.add("B");
        linkedList.add("C");
        linkedList.add("D");
        assertEquals(4, linkedList.size());
    }
}
