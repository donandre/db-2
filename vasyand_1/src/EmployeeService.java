public class EmployeeService {
    Employee[] employees;

    void setEmployees(Employee[] newEmployees) {
        employees = newEmployees;
    }

    public EmployeeService(Employee[] employees) {
        this.employees = employees;
    }

    void print() {
        for (Employee employee : employees) {
            employee.print();
            System.out.println();
        }
    }

    double calculateSalary() {
        double result = 0.0;
        for (Employee employee : employees) {
            result += employee.salary;
        }
        return result;
    }

    void removeEmployee(int id) {
        int matchCnt = 0;
        for (Employee employee : employees) {
            if (employee.id == id) {
                matchCnt++;
            }
        }
        if (matchCnt > 0) {
            Employee[] editedEmployees = new Employee[matchCnt];
            for (int i = 0, j = 0; i < employees.length; i++) {
                if (employees[i].id != id) {
                    editedEmployees[j] = employees[i];
                    j++;
                }
            }
            employees = editedEmployees;
        }
    }

    void addEmployee(Employee employee) {
        Employee[] newEmployees = new Employee[employees.length + 1];
        for (int i = 0; i < employees.length; i++) {
            newEmployees[i] = employees[i];
        }

        newEmployees[newEmployees.length - 1] = employee;
        employees = newEmployees;
    }

    Employee[] sortByLastName() {
        return sortByLastName(employees);
    }

    Employee[] sortByLastName(Employee[] input) {
        for (int i = 0; i < input.length - 1; i++) {
            if (input[i].Name.compareTo(input[i + 1].Name) > 0) {
                Employee temp = input[i + 1];
                input[i + 1] = input[i];
                input[i] = temp;
                sortByLastName(input);
            }
        }
        return input;
    }

    Employee[] sortByLastFirstName() {
        return sortByLastFirstName(employees);
    }

    Employee[] sortByLastFirstName(Employee[] input) {
        String first;
        String second;
        int n = input.length;
        Employee temp;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                first = input[j - 1].Name + input[j - 1].Name;
                second = input[j].Name + input[j].Name;
                if (first.compareTo(second) > 0) {
                    temp = input[j - 1];
                    input[j - 1] = input[j];
                    input[j] = temp;
                }

            }
        }
        return input;
    }

}