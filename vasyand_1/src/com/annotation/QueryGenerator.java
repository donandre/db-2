package com.annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.StringJoiner;

// "SELECT person_id, person_name, salary FROM Persons;";
public class QueryGenerator {

    public String getAll(Class clazz) {
        Annotation classAnnotation = clazz.getAnnotation(Table.class);
        Table tableAnnotation = (Table) classAnnotation;

        if (tableAnnotation == null) {
            throw new IllegalArgumentException("@Table is not present");
        }

        String tableName = tableAnnotation.name().isEmpty() ?
                clazz.getName() : tableAnnotation.name();

        // person_id, person_name, salary
        StringJoiner columns = new StringJoiner(", ");
        for (Field field : clazz.getDeclaredFields()) {
            Column columnAnnotation = field.getAnnotation(Column.class);
            if (columnAnnotation != null) {
                String columnName = columnAnnotation.name().isEmpty() ?
                        field.getName() : columnAnnotation.name();
                columns.add(columnName);
            }
        }

        StringBuilder stringBuilder = new StringBuilder("SELECT ");
        stringBuilder.append(columns.toString());
        stringBuilder.append(" FROM ");
        stringBuilder.append(tableName);
        stringBuilder.append(";");

        return stringBuilder.toString();
    }

    public String insert(Object value) {
        Person person = (Person) value;
        Class clazz = person.getClass();
        Annotation classAnnotation = clazz.getAnnotation(Table.class);
        Table tableAnnotation = (Table) classAnnotation;

        if (tableAnnotation == null) {
            throw new IllegalArgumentException("@Table is not present");
        }

        String tableName = tableAnnotation.name().isEmpty() ?
                clazz.getName() : tableAnnotation.name();

        // person_id, person_name, salary
        StringJoiner columns = new StringJoiner(", ");
        for (Field field : clazz.getDeclaredFields()) {
            Column columnAnnotation = field.getAnnotation(Column.class);
            if (columnAnnotation != null) {
                String columnName = columnAnnotation.name().isEmpty() ?
                        field.getName() : columnAnnotation.name();
                columns.add(columnName);
            }
        }

        StringBuilder stringBuilder = new StringBuilder("INSERT INTO ");
        stringBuilder.append(tableName);
        stringBuilder.append(" (");
        stringBuilder.append(columns.toString());
        stringBuilder.append(") VALUES (");
        stringBuilder.append(person.id);
        stringBuilder.append(", '");
        stringBuilder.append(person.name);
        stringBuilder.append("', ");
        stringBuilder.append((int)person.salary);
        stringBuilder.append(");");
        return stringBuilder.toString();
    }

    public String getById(Class clazz, Object id) {
        StringBuilder outerQuery = new StringBuilder(getAll(clazz));
        if (outerQuery.length() > 0) {
            outerQuery = outerQuery.deleteCharAt(outerQuery.lastIndexOf(";"));
            Field field = null;
            try {
                field = clazz.getDeclaredField("id");
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            Column columnAnnotation = field.getAnnotation(Column.class);
            if (columnAnnotation != null) {
                String columnName = columnAnnotation.name().isEmpty() ?
                        field.getName() : columnAnnotation.name();
                outerQuery.append(" WHERE ");
                outerQuery.append(columnName);
                outerQuery.append(" = ");
                outerQuery.append(id);
                outerQuery.append(";");
            }
        }

        return outerQuery.toString();
    }

    public String delete(Class clazz, Object id) {
        Annotation classAnnotation = clazz.getAnnotation(Table.class);
        Table tableAnnotation = (Table) classAnnotation;
        if (tableAnnotation == null) {
            throw new IllegalArgumentException("@Table is not present");
        }
        StringBuilder outerQuery = new StringBuilder();
        String tableName = tableAnnotation.name().isEmpty() ?
                clazz.getName() : tableAnnotation.name();

        Field field = null;
        try {
            field = clazz.getDeclaredField("id");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        Column columnAnnotation = field.getAnnotation(Column.class);
        if (columnAnnotation != null) {
            String columnName = columnAnnotation.name().isEmpty() ?
                    field.getName() : columnAnnotation.name();
            outerQuery.append("DELETE FROM ");
            outerQuery.append(tableName);
            outerQuery.append(" WHERE ");
            outerQuery.append(columnName);
            outerQuery.append(" = ");
            outerQuery.append(id);
            outerQuery.append(";");
        }
        return outerQuery.toString();
    }
}
