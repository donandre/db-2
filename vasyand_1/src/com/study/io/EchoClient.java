package com.study.io;


import java.io.*;
import java.net.Socket;
import java.nio.Buffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class EchoClient {
    Socket socket;
    InputStream input;
    OutputStream output;


    public InputStream getInput() {
        return input;
    }

    public OutputStream getOutput() {
        return output;
    }

    public Socket getSocket() {
        return socket;
    }

    void openConnection(String hostname, int port) throws IOException {
        closeConnectionIfConnected();
        socket = new Socket(hostname, port);
        input = socket.getInputStream();
        output = socket.getOutputStream();
    }

    void closeConnectionIfConnected() throws IOException {
        if (socket != null) {
            socket = null;
        }
        if (input != null) {
            input = null;
        }
        if (output != null) {
            output = null;
        }
    }

    void writeMessageToOutputStream(List<String> message) throws IOException {
        BufferedWriter bufferedOutput = new BufferedWriter(new OutputStreamWriter(output));
        for (String line : message) {
            bufferedOutput.write(line+"\n");
        }
        bufferedOutput.write("\n");
        bufferedOutput.flush();
    }

    List getMessageFromInputStream() throws IOException {

        BufferedReader bufferedInput = new BufferedReader(new InputStreamReader(input));
        List<String> inputLines = new ArrayList<>();
        String nextLines = "";
        while ((nextLines = bufferedInput.readLine()) != null && !nextLines.isEmpty()) {
            inputLines.add(nextLines);
        }
        return inputLines;
    }

    public boolean isConnected() {
        return socket != null && socket.isConnected();
    }

    public EchoClient(Socket socket) throws IOException {
        if (socket != null) {
            this.socket = socket;
            input = socket.getInputStream();
            output = socket.getOutputStream();
        }

    }

    public EchoClient() {
    }


    public static void main(String[] args) throws IOException {
        String userInput;
        EchoClient client;
        while (System.in.available() > -1) {
            System.out.println("enter message to server or STOP to exit");
            Scanner lineScanner = new Scanner(System.in);
            userInput = (lineScanner.nextLine());
            if (userInput.equals("STOP")) {
                break;
            }
            List<String> message = new ArrayList<>();
            message.add(userInput);
            client = new EchoClient();
            client.openConnection("localhost", 123);
            if (client.isConnected()) {
                client.writeMessageToOutputStream(message);
                System.out.println("Response from server: " + client.getMessageFromInputStream().toString());
            }
            client.closeConnectionIfConnected();
        }
    }
}
