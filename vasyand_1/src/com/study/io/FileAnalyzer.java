package com.study.io;

import java.io.*;
import java.util.List;

public class FileAnalyzer {
    private static List<String> getStringLinesFromFile(String pathToFile) throws IOException {
        File inputFile = new File(pathToFile);
        List<String> records = new java.util.ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(inputFile));
        String line;
        while ((line = reader.readLine()) != null) {
            //need to improve regex
            //String b= "This is first sentence with is. This is next sentence. This is third.";
            //String regex = "[^.?!]*[.?!]";
            //String [] a = b.split(regex); //0 matches
            int substrBegin = 0;
            int substrEnd;
            char[] lineChars = line.toCharArray();
            for (int i = 0; i < lineChars.length; i++) {
                if (lineChars[i] == '.' || lineChars[i] == '!' || lineChars[i] == '?') {
                    substrEnd = i;
                    records.add(line.substring(substrBegin, substrEnd).trim());
                    substrBegin = i + 1;
                }
            }

        }
        reader.close();
        return records;
    }

    private static void printSubstringLines(List<String> stringPool, String pattern) {
        for (String line : stringPool) {
            if (line.contains(pattern)) {
                System.out.println(line);
            }
        }
    }

    private static void printSubstringCounts(List<String> stringPool, String pattern) {
        int counter = 0;
        for (String line : stringPool) {
            counter = counter + line.split(pattern).length - 1;
        }
        System.out.println("foud total " + counter + " matches of " + pattern);

    }


    public static void main(String[] args) throws IOException {
        String pathToFile = args[0];
        String searchString = args[1];
        List<String> fileLines = getStringLinesFromFile(pathToFile);
        printSubstringLines(fileLines, searchString);
        printSubstringCounts(fileLines, searchString);

    }
}
