package com.study.io;


import java.io.IOException;
import java.io.InputStream;


public class BufferedInputStream extends InputStream {
    private InputStream inputStream;
    private byte[] buffer = new byte[1024];
    private int index;
    private int count;


    public BufferedInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }


    @Override
    public int read() throws IOException {
        if (index == count) {
            count = inputStream.read(buffer);
            index = 0;
        }


        if (count == -1) {
            return -1;
        }
        int value = buffer[index];
        index++;


        return value;
    }


    @Override
    public int read(byte[] b) throws IOException {
        // read from buffer with array copy
        return super.read(b);
    }


    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        // read from buffer with array copy
        return super.read(b, off, len);
    }


    @Override
    public void close() throws IOException {
        inputStream.close();
    }
}

