package com.study.io;
/*
Метод принимает object и вызывает у него все методы без параметров//
        Метод принимает object и выводит на экран все сигнатуры методов в ко//торых есть final
        Метод принимает Class и возвращает список всех предков класса и все интерфейсы которое класс имплементирует
        Метод принимает объект и меняет всего его поля на их нулевые значение (null, 0, false etc)+
*/

import com.collections.ArrayList;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionTest {
    public static void main(String[] args) throws Exception {

        //runAllMethods(new InvokeClass());
        printFinalSignatures(new InvokeClass());

    }

    static void runAllMethods(Object targetObject) throws InvocationTargetException, IllegalAccessException {
        Class targetClass = targetObject.getClass();
        for (Method method : targetClass.getDeclaredMethods()) {
            method.setAccessible(true);
            method.invoke(targetObject);
        }
    }

    static void printFinalSignatures(Object targetObject) throws InvocationTargetException, IllegalAccessException {
        Class targetClass = targetObject.getClass();

        for (Method method : targetClass.getDeclaredMethods()) {
            if (method.isAccessible()){ //?
                System.out.println(method.getName());
            }
        }
    }

    static ArrayList getClassChilds(Class targetClass){
        ArrayList result = new ArrayList();
        for (Class childClass:targetClass.getClasses()) {
            result.add(childClass.getClasses());
            //result.add(getClassChilds());
        }
        return result;
    }
    static void runTests(String testClassName) throws Exception {
        Class clazz = Class.forName(testClassName);
        Object instance = clazz.newInstance(); // new HashMapTest()

        for (Method method : clazz.getMethods()) {
            if (method.getName().startsWith("test")) {
                method.invoke(instance);
            }
        }
    }

    static void process(Object obj) throws Exception {
        Class clazz = obj.getClass();
        for (Field field : clazz.getDeclaredFields()) {
            String newName = "John bad guy!";
            if (field.getName().equals("value")) {
                field.setAccessible(true);
                field.set(obj, newName.toCharArray());
                field.setAccessible(false);
            }
        }
    }


}