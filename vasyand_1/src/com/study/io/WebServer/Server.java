package com.study.io.WebServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public interface Server {
    void doServerProcessing(Socket clientSocket) throws IOException;
    void setPort(int port); //3000
    void setWebAppPath(String path); //"resource/webapp"
    void start();
    void stop();
    boolean isStarted();

}
