package com.study.io.WebServer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class ResponseWriter {
    private String httpVersion;
    private OutputStream outputStream;
    private HashMap<String, String> headers;


    void sendSuccessResponse(File outputFile) throws IOException {

        outputStream.write((httpVersion + " " + HttpStatusCode.OK.asText()).getBytes());
        outputStream.write("\n".getBytes());
        insertHeaders();
        outputStream.write("\n".getBytes());
        FileInputStream sourceFile = new FileInputStream(outputFile);
        int cursor;
        while ((cursor = sourceFile.read()) != -1) {
            outputStream.write(cursor);
        }
        outputStream.write("\n".getBytes());
        outputStream.write("\n".getBytes());
        outputStream.flush();
        outputStream.close();

    }

    void sendNotFoundResponse() throws IOException {
        outputStream.write((httpVersion + " " + HttpStatusCode.NOT_FOUND.asText()).getBytes());
        outputStream.write("\n".getBytes());
        insertHeaders();
        outputStream.write("\n".getBytes());
        outputStream.flush();
        outputStream.close();
    }

    private void insertHeaders() throws IOException {
        if (headers.size() > 0) {
            Set entrySet = headers.entrySet();
            Iterator it = entrySet.iterator();
            while (it.hasNext()) {
                Map.Entry me = (Map.Entry) it.next();
                outputStream.write((me.getKey() + ": " + me.getValue()).getBytes());
                outputStream.write("\n".getBytes());
            }
        }
    }

    public ResponseWriter(OutputStream os) {
        outputStream = os;
        httpVersion = "HTTP/1.1";
        headers = new HashMap<String, String>();
        fillHeaders();
    }

    private void fillHeaders() {
        headers.put("Server", "WebServer_refactored");
        headers.put("Cache-Control", "no-cache");
    }


}
