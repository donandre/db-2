package com.study.io.WebServer;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class WebServer implements Server {
    private ServerSocket serverSocket;
    private int port;
    private String path;

    public WebServer() {
        this.path = "/";
    }

    void writeFileToOutputStream(File inputFile, OutputStream outputStream) throws IOException {
        FileInputStream sourceFile = new FileInputStream(inputFile);
        int cursor;
        while((cursor = sourceFile.read())!=-1){
            outputStream.write(cursor);
        }
    }

    void writeMessageToOutputStream(List<String> message, OutputStream outputStream) throws IOException {
        BufferedWriter bufferedOutput = new BufferedWriter(new OutputStreamWriter(outputStream));
        for (String line : message) {
            outputStream.write(line.getBytes());
            outputStream.write("\n".getBytes());
        }
        //bufferedOutput.flush();
        //bufferedOutput.close();
    }

    List<String> getMessageFromInputStream(InputStream inputStream) throws IOException {
        BufferedReader bufferedInput = new BufferedReader(new InputStreamReader(inputStream));
        List<String> inputLines = new ArrayList<>();
        String nextLines = "";
        while ((nextLines = bufferedInput.readLine()) != null && !nextLines.isEmpty()) {
            inputLines.add(nextLines);
        }
        return inputLines;
    }

    String getRequestedFilePath(InputStream inputStream) throws IOException {
        String result = "";
        for (String line : getMessageFromInputStream(inputStream)) {
            if (line.startsWith("GET")) {
                result = line.split(" ")[1];
                break;
            }
        }
        return result.trim();
    }



    public void doServerProcessing(Socket clientSocket) throws IOException {
        if (isStarted()) {
            List<String> response = new ArrayList<>();
            String httpVersion = "HTTP/1.1";


            File requestedFile = new File(path + getRequestedFilePath(clientSocket.getInputStream()));
            if (!requestedFile.exists()){
              response.add(httpVersion+" "+ HttpStatusCode.NOT_FOUND.asText());
              response.add("");
              writeMessageToOutputStream(response, clientSocket.getOutputStream());
                clientSocket.getOutputStream().flush();
                clientSocket.getOutputStream().close();
            }
            else {
                response.add(httpVersion+" "+HttpStatusCode.OK.asText());
                response.add("");
                writeMessageToOutputStream(response, clientSocket.getOutputStream());
                writeFileToOutputStream(requestedFile, clientSocket.getOutputStream());
                clientSocket.getOutputStream().flush();
                clientSocket.getOutputStream().close();
            }
        }
    }


    public void setPort(int port) {
        this.port = port;
    }


    public void setWebAppPath(String path) {
        this.path = new File("").getAbsolutePath() + path;
    }


    public void start() {
        if (canBeStarted()) {
            try {
                serverSocket = new ServerSocket(port);
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                int i = 0;
                while (i < 10000) {
                    Socket clientSocket = serverSocket.accept();
                    doServerProcessing(clientSocket);
                    i++;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean canBeStarted() {
        return (port > 0);
    }

    public void stop() {

    }


    public boolean isStarted() {
        return true;
    }

    public int getPort() {
        return port;
    }

    public String getPath() {
        return path;
    }
}
