package com.study.io.WebServer;

import java.io.*;

public class RequestHandler {
    private BufferedReader reader;
    private OutputStream writer;
    private String webAppPath;

    void handle() {
        RequestParser requestParser = new RequestParser(reader);
        try {
            Request request = requestParser.parseRequest();

            if (request.getHttpMethod().equals("GET")) {
                ResponseWriter response = new ResponseWriter(writer);
                File file = new File(webAppPath + request.getURI());
                if (!file.exists()) {
                    response.sendNotFoundResponse();
                } else {
                    response.sendSuccessResponse(file);
                }

            }
        } catch (IOException e) {

            e.printStackTrace();
        }


    }

    public BufferedReader getReader() {
        return reader;
    }

    public OutputStream getWriter() {
        return writer;
    }

    public String getWebAppPath() {
        return webAppPath;
    }

    public RequestHandler(BufferedReader reader, OutputStream writer, String webAppPath) {
        this.reader = reader;
        this.writer = writer;
        this.webAppPath = new File("").getAbsolutePath() + webAppPath;
    }
}
