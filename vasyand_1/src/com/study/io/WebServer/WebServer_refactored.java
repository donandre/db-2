package com.study.io.WebServer;


import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class WebServer_refactored {
    private int port;
    private String path;


    public void start() {
        try {
            ServerSocket serverSocket = new ServerSocket(port);

            while (true) {
                Socket clientSocket = serverSocket.accept();
                BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                RequestHandler requestHandler = new RequestHandler(br, clientSocket.getOutputStream(), path);
                requestHandler.handle();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public WebServer_refactored() {
    }

    public WebServer_refactored(int port, String path) {
        this.port = port;
        this.path = path;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
