package com.study.io.WebServer;

import java.io.BufferedReader;
import java.io.IOException;


public class RequestParser {
    BufferedReader bufferedReader;

    Request parseRequest() throws IOException {
        Request request = new Request();
        String nextLines = "";
        boolean firstLine = true;
        String[] line;
        while ((nextLines = bufferedReader.readLine()) != null && !nextLines.isEmpty()) {

            if (!firstLine){
               line = nextLines.split(": ");
               request.addHeader(line[0], line[1]);
            }
            else {
                line= nextLines.split(" ");
                request.setHttpMethod(line[0]);
                request.setURI(line[1]);
                firstLine = false;
            }
        }
        return request;
    }

    public RequestParser(BufferedReader bufferedReader) {
        this.bufferedReader = bufferedReader;
    }
}
