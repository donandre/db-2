package com.study.io.WebServer;

import java.util.HashMap;
import java.util.Map;

public class Request {
    private String URI;
    private String HttpMethod;
    private HashMap<String, String> headers;

    public Request(String URI, String httpMethod, HashMap<String, String> headers) {
        this.URI = URI;
        HttpMethod = httpMethod;
        this.headers = headers;
    }

    public Request() {
        headers = new HashMap<String, String>();
    }

    public void addHeader(String key, String value) {
        headers.put(key, value);
    }

    public void setURI(String URI) {
        this.URI = URI;
    }

    public void setHttpMethod(String httpMethod) {
        HttpMethod = httpMethod;
    }

    public void setHeaders(HashMap<String, String> headers) {
        this.headers = headers;
    }

    public String getURI() {
        return URI;
    }

    public String getHttpMethod() {
        return HttpMethod;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }
}
