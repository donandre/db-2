package com.study.io;


import org.junit.Before;

import java.io.*;

public class FileManager {


    public static int countFiles(String path) {
        int counter = 0;
        File rootPath = new File(path);
        if (!rootPath.exists()){
            return counter;
        }
        for (File innerPath : rootPath.listFiles()) {
            if (innerPath.isFile()) {
                counter++;
            } else {
                counter += countFiles(innerPath.getAbsolutePath());
            }

        }
        return counter;
    }

    public static int countDirs(String path) {
        int counter = 0;
        File rootPath = new File(path);
        if (!rootPath.exists()){
            return counter;
        }
        for (File innerPath : rootPath.listFiles()) {
            if (innerPath.isDirectory()) {
                counter++;
                counter += countDirs(innerPath.getAbsolutePath());
            }

        }
        return counter;
    }

    public static void copyDir(String pathFrom, String pathTo) {
        copyFolder(new File(pathFrom), new File(pathTo), false);

    }
    public static void moveDir(String pathFrom, String pathTo){
        copyFolder(new File(pathFrom), new File(pathTo), true);
    }

    private static void copyFolder(File sourceDir, File destinationDir, boolean removeSource) {
        if (sourceDir.isDirectory()) {
            if (!destinationDir.exists()) {
                destinationDir.mkdirs();
            }

            String[] files = sourceDir.list();

            for (String file : files) {
                File srcFile = new File(sourceDir, file);
                File destFile = new File(destinationDir, file);

                copyFolder(srcFile, destFile, removeSource);
                //delete source dir;

            }
            if (removeSource) {
                sourceDir.delete();
            }
        } else {
            InputStream in = null;
            OutputStream out = null;

            try {
                in = new FileInputStream(sourceDir);
                out = new FileOutputStream(destinationDir);

                byte[] buffer = new byte[1024];

                int length;
                while ((length = in.read(buffer)) > 0) {
                    out.write(buffer, 0, length);

                }
                if (removeSource) {
                    sourceDir.delete();
                }
            } catch (Exception e) {
                try {
                    in.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                try {
                    out.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

}