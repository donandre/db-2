package com.study.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;

public class RequestHandler {
    private BufferedReader bufferedReader;
    private BufferedWriter bufferedWriter;
    String webAppPath;

    void handle() {

    }

    public RequestHandler(BufferedReader bufferedReader, BufferedWriter bufferedWriter, String webAppPath) {
        this.bufferedReader = bufferedReader;
        this.bufferedWriter = bufferedWriter;
        this.webAppPath = webAppPath;
    }

    public BufferedReader getBufferedReader() {
        return bufferedReader;
    }

    public BufferedWriter getBufferedWriter() {
        return bufferedWriter;
    }

    public String getWebAppPath() {
        return webAppPath;
    }
}
