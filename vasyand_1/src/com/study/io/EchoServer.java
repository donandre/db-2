package com.study.io;

import java.util.ArrayList;


import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;

public class EchoServer {
    static void doServerProcessing(InputStream inputStream, OutputStream outputStream) throws IOException {
        BufferedReader bufferedInput = new BufferedReader(new InputStreamReader(inputStream));
        BufferedWriter bufferedOutput = new BufferedWriter(new OutputStreamWriter(outputStream));
        List<String> inputLines = new ArrayList<>();
        String nextLines = "";
        while ((nextLines = bufferedInput.readLine()) != null && !nextLines.isEmpty()) {
            inputLines.add(nextLines);
        }
        try {
            bufferedOutput.write("Echo ");
            for (String line : inputLines) {
                bufferedOutput.write(line + "\n");
            }
            bufferedOutput.write("\n");
            bufferedOutput.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(123);
        while (true) {
            try {
                System.out.println("Waiting for client connection");
                Socket clientSocket = serverSocket.accept();
                System.out.println("Client connected");
                doServerProcessing(clientSocket.getInputStream(), clientSocket.getOutputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
