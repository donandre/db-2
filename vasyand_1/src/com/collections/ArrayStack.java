package com.collections;

import java.util.NoSuchElementException;

public class ArrayStack <V> implements Stack {

    V[] array;
    int size;

    public ArrayStack() {
        array = (V[]) new Object[5];
    }

    public void push(Object value) {
        // grow
        if (size == array.length) {
            V[] newArray = (V[]) new Object[array.length * 3 / 2];
            for (int i = 0; i < array.length; i++) {
                newArray[i] = array[i];
            }
            array = (V[]) newArray;
        }
        // add
        array[size] = (V) value;
        size++;
    }

    public Object pop() {
        if (size == 0) {
            NoSuchElementException noSuchElementException = new NoSuchElementException();
            throw noSuchElementException;
        }
        V result = array[size - 1];
        size--;
        return result;
    }

    public Object peek() {
        if (size == 0) {
            throw new NoSuchElementException();
        }
        return array[size - 1];
    }

    public int size() {
        return size;
    }

    @Override
    public boolean contains(Object value) {
        return false;
    }
}