package com.collections;

public class LinkedListNode {
    Object value;
    LinkedListNode next;
    LinkedListNode previous;

    public LinkedListNode(Object value) {
        this.value = value;
    }

    public LinkedListNode() {
    }
}
