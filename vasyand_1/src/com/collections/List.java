package com.collections;

import java.util.Iterator;

public interface List <V> {
    // add value to the end of the list
    void add(V value);

    // we can add value by index between [0, size]
    // otherwise throw new IndexOutOfBoundsException
    // [A, B, C] . add("D", [0, 1, 2, 3])

    void add(V value, int index);
    // we can remove value by index between [0, size - 1]
    // otherwise throw new IndexOutOfBoundsException

    // [A, B, C] remove = 0
    // [B (index = 0) , C (index = 1)]
    V remove(int index);

    // [A, B, C] size = 3
    // we can get value by index between [0, size - 1]
    // otherwise throw new IndexOutOfBoundsException
    V get(int index);

    // we can set value by index between [0, size - 1]
    // otherwise throw new IndexOutOfBoundsException
    V set(V value, int index);

    void clear();

    int size();

    boolean isEmpty();

    boolean contains(V value);

    int indexOf(V value);

    int lastIndexOf(V value);

    // [A, B, C]
    String toString();

    Iterator iterator();
}
