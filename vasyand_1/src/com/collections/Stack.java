package com.collections;

public interface Stack <V> {
    void push(V value);

    V pop();

   V peek();

    int size();

    boolean contains(V value);
}