package com.collections;

public class LinkedQueue implements Queue {
    int size;
    Node head;

    @Override
    public void enqueue(Object value) {
        Node newNode = new Node(value);
        if (size == 0) {
            head = newNode;
        } else {
            Node current = head;
            while (current.next != null) {
                current = current.next;
            }
            current.next = newNode;
        }

        size++;
    }

    @Override
    public Object dequeue() {
        Object value = head.value;
        head = head.next;
        size--;
        return value;
    }

    @Override
    public Object peek() {
        return head.value;
    }

    @Override
    public int size() {
        return size;
    }
}