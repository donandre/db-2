package com.collections;

import java.util.Iterator;
import java.util.Objects;

public class HashMap implements Iterable {
    private static int BUCKET_SIZE = 3;
    final static double LOAD_FACTOR = 0.5;
    ArrayList[] buckets = new ArrayList[BUCKET_SIZE];

    public void checkLoad() {
        if (BUCKET_SIZE * LOAD_FACTOR < size()) {
            grow();
        }
    }

    public void grow() {
        HashMap oldMap = this;
        BUCKET_SIZE = (int) (BUCKET_SIZE / LOAD_FACTOR);
        putAll(oldMap);
    }

    public void putAll(HashMap map) {
        Iterator oldMap = map.iterator();
        while (oldMap.hasNext()) {
            Entry entry = (Entry) oldMap.next();
            this.putIfAbsent(entry.key, entry.value);
        }
    }


    public Object put(Object key, Object value) {

        Entry oldEntry = setItem(key, value, true);

        return getValue(oldEntry);
    }


    public Object putIfAbsent(Object key, Object value) {
        Entry oldEntry = setItem(key, value, false);

        return getValue(oldEntry);
    }


    public Object get(Object key) {
        Entry entry = getEntry(key);

        return getValue(entry);
    }


    public Object remove(Object key) {
        List bucket = getBucket(key);
        Object oldValue = null;

        Iterator iterator = bucket.iterator();
        // remove

        while (iterator.hasNext()) {
            Entry entry = (Entry) iterator.next();

            if (entry.key.equals(key)) {
                oldValue = entry.value;
                iterator.remove();
                break;
            }
        }

        return oldValue;
    }

    //
    public int size() {
        int size = 0;
        for (int i = 0; i < BUCKET_SIZE; i++) {
            if (buckets[i] != null) {
                size += buckets[i].size;
            }
        }
        return size;
    }


    public boolean containsKey(Object key) {
        return getEntry(key) != null;
    }


    public Iterator iterator() {
        return new MapIterator();
    }


    private int bucketIndex(Object key) {
        return Math.abs(key.hashCode() % BUCKET_SIZE);
    }


    private ArrayList getBucket(Object key) {
        int index = bucketIndex(key);
        ArrayList result = buckets[index];
        if (result == null) {
            result = new ArrayList();
            buckets[index] = result;
        }
        return result;
    }


    private Entry getEntry(Object key) {
        ArrayList bucket = getBucket(key);
        for (Object o : bucket) {
            Entry entry = (Entry) o;
            if (entry!=null&&entry.key.equals(key)) {
                return entry;
            }
        }
        return null;
    }

    // return old item
    private Entry setItem(Object key, Object value, boolean forceUpdate) {
        checkLoad();
        Entry result = null;
        Entry oldEntry = getEntry(key);
        if (oldEntry != null) {
            if (forceUpdate) {
                result = new Entry(oldEntry.key, oldEntry.value);
                // update value
                oldEntry.value = value;
            }
        } else {
            // key not found
            ArrayList bucket = getBucket(key);
            Entry newEntry = new Entry(key, value);
            bucket.add(newEntry);
            if (!forceUpdate) {
                result = newEntry;
            }
        }
        return result;
    }

    private Object getValue(Entry entry) {
        if (entry == null) {
            return null;
        }
        return entry.value;
    }


    private class MapIterator implements Iterator {
        private int index;
        private Iterator iterator = buckets[index].iterator();


        public boolean hasNext() {
            boolean result = iterator.hasNext();
            if (!result && index + 1 < BUCKET_SIZE) {
                result = buckets[index + 1].iterator().hasNext();
            }
            return result;
        }


        public Object next() {
            Object item = null;
            if (iterator.hasNext()) {
                item = iterator.next();
            }
            if (!iterator.hasNext() && index + 1 < BUCKET_SIZE) {
                index++;
                iterator = buckets[index].iterator();
                if (iterator.hasNext()) {
                    item = iterator.next();
                }
            }
            return ((Entry) item).value;
        }
    }

    private static class Entry {
        Object key;
        Object value;

        public Entry(Object key, Object value) {
            this.key = key;
            this.value = value;
        }

        public int hashCode() {
            return Objects.hashCode(key) ^ Objects.hashCode(value);
        }

        public final boolean equals(Object o) {
            if (o == this)
                return true;
            if (o instanceof Entry) {
                Entry entry = (Entry) o;
                if (Objects.equals(key, entry.key) && Objects.equals(value, entry.value)) {
                    return true;
                }
            }
            return false;
        }
    }
}