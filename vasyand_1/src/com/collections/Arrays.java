package com.collections;

public class Arrays {


    public static void main(String[] args) {

        int[] array2 = {2, 1, 4, 5, 7, 8, 10};
        char[] chrArray = {'a', 'f', 'T', 'k'};
        getMultiplyElements(array2, 4);
    }

    static String arrayToString(char[] input) {
        String out = "";
        for (int i = 0; i < input.length; i++) {
            out = out + input[i];
        }
        return out;
    }

    static int getFirstIndex(int[] input, int value) {
        for (int i = 0; i < input.length; i++) {
            if (input[i] == value) {
                return i + 1;
            }
        }
        return -1;
    }

    static int lastIndexOf(int[] input, int value) {
        for (int i = input.length - 1; i >= 0; i--) {
            if (input[i] == value) {
                return i;
            }
        }
        return -1;
    }

    static void getMultiplyElements(int[] input, int value) {
        for (int i = 0; i < input.length; i++) {
            if (input[i] % value == 0) {
                System.out.println(" " + input[i]);
            }
        }
    }

    static void sortArray(int[] input) {
        for (int i = 0; i < input.length - 1; i++) {
            int currNum;
            if (input[i] > input[i + 1]) {
                currNum = input[i + 1];
                input[i + 1] = input[i];
                input[i] = currNum;
                sortArray(input);
                return;
            }
        }
        print(input);

    }

    static boolean isByteEquals(byte[] input) {
        for (int i = 0; i < input.length - 1; i++) {
            for (int j = 0; j < input.length - 1 - i; j++) {
                if (input[j] == input[j + 1]) {
                    return true;
                }
            }
        }
        return false;
    }

    static String[] findStringMatches(String[] input, String key) {
        int cnt = 0;
        for (int i = 0; i < input.length; i++) {
            if (input[i].indexOf(key) > 0) {
                cnt++;
            }
        }
        if (cnt > 0) {
            String[] output = new String[cnt];
            for (int i = 0, j = 0; i < input.length; i++) {
                if (input[i].indexOf(key) > 0) {
                    output[j] = input[i];
                    j++;
                }
            }
            return output;
        } else return new String[0];
    }


    static void print(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int value = array[i];
            System.out.println(value);
            // System.out.print(array[i]);
        }
    }

    static int[] generateArray(int n) {
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = i + 1;
        }

        return array;
    }

    static void m() {
        int i = 10;
        int k = i;
    }
}