package com.collections;

import java.util.Objects;

public class LinkedStack implements Stack {
    int size;
    Node tail;
    @Override
    public void push(Object value) {

        Node newNode = new Node(value);
        if (size == 0) {
            tail = newNode;
        } else {
            Node previoous = tail;
            tail=newNode;
            tail.next=previoous;
        }
        size++;

    }

    @Override
    public Object pop() {
        Object value = tail.value;
        tail=tail.next;
        size--;
        return value;
    }

    @Override
    public Object peek() {
        return tail.value;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean contains(Object value) {
        Node current = tail;
        for (int i = 0; i < size; i++) {
            if(Objects.equals(current.value, value)){
                return true;
            }
            current=tail.next;

        }
        return false;
    }
}
