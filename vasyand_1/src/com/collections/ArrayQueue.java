package com.collections;

import java.util.NoSuchElementException;

public class ArrayQueue <V> implements Queue {

    private V[] array;
    private int size = 0;

    public ArrayQueue() {
        array = (V[]) new Object[5];
    }

    @Override
    public void enqueue(Object value) {
        if (size == array.length) {
            V[] newArray = (V[])new Object[(size) * 3 / 2];
            for (int i = 0; i < size; i++) {
                newArray[i] = array[i];
            }
            array = newArray;
        }
        array[size] = (V) value;
        size++;
    }

    @Override
    public V dequeue() {
        if (size == 0) {
            NoSuchElementException noSuchElementException = new NoSuchElementException();
            throw noSuchElementException;
        }
        V value = array[0];
        System.arraycopy(array, 1, array, 0, size - 1);
        size--;
        return value;
    }

    @Override
    public V peek() {
        if (size == 0) {
            NoSuchElementException noSuchElementException = new NoSuchElementException();
            throw noSuchElementException;
        }
        return array[0];
    }

    @Override
    public int size() {
        return size;
    }
}