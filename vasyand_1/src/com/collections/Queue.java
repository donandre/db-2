package com.collections;

public interface Queue <T> {
    void enqueue(Object value);

    T dequeue();

    T peek();

    int size();
}