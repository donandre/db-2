import java.util.Random;

public class MockEmployeesGenerator {
    static Employee[] generate(int size) {


        Employee currentEmployee = null;
        Employee[] randomEmployees = new Employee[size];

        String[] firstNames = {"John", "William", "Debra", "Lucy", "Philip", "Stanley", "Alex"};
        String[] lastNames = {"Defoe", "Rabinovich", "Malkin", "Smith", "O'Connor", "Blazkovich", "Cooney"};
        Random random = new Random();
        int role;
        for (int i = 0; i < size; i++) {
            role = random.nextInt(3);
            if (role == 0) {
                currentEmployee = new Developer();
                ((Developer) currentEmployee).FixedBugs=0;
            } else if (role == 1) {
                currentEmployee = new Manager();
            } else if (role == 2) {
                currentEmployee = new Cleaner();
                ((Cleaner) currentEmployee).rate = random.nextInt(100);
                ((Cleaner) currentEmployee).workedDays = 0;
            }
            String firstName = firstNames[random.nextInt(firstNames.length - 1)];
            String lastName = lastNames[random.nextInt(lastNames.length - 1)];
            currentEmployee.id = i + 1;
            currentEmployee.Name = firstName + " " + lastName;
            currentEmployee.age = random.nextInt(100);
            currentEmployee.salary = random.nextDouble();
            char sex;
            if (random.nextBoolean()) {
                sex = 'M';
            } else {
                sex = 'M';
            }
            currentEmployee.gender = sex;

            randomEmployees[i] = currentEmployee;

        }
        return randomEmployees;
    }
}