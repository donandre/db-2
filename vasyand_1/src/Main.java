import com.study.io.WebServer.WebServer;
import com.study.io.WebServer.WebServer_refactored;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


public class Main {
    public static void main(String[] args) {

        HashMap <String, String> hm = new HashMap<>();
        hm.put("1","ONE");
        hm.put("2","TWO");
        hm.put("100","ONE HUNDRED");
        hm.put("30","THIRTY");
        hm.put("T","Just T letter");


        // Getting a Set of Key-value pairs
        Set entrySet = hm.entrySet();

        // Obtaining an iterator for the entry set
        Iterator it = entrySet.iterator();

        // Iterate through HashMap entries(Key-Value pairs)
        System.out.println("HashMap Key-Value Pairs : ");
        while(it.hasNext()){
            Map.Entry me = (Map.Entry)it.next();
            System.out.println("Key is: "+me.getKey() +
                    " & " +
                    " value is: "+me.getValue());
        }


        WebServer_refactored ws = new WebServer_refactored();
        ws.setPath("/resources/webapp/");
        ws.setPort(3000);
        ws.start();
    }
}