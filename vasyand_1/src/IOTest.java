import java.io.File;
import java.io.IOException;
public class IOTest {
    public static void main(String[] args) throws IOException {

        File path = new File("C:\\Windows");
        System.out.println("isDirectory: " + path.isDirectory());

        for (File innerPath : path.listFiles()) {
            String prefix = innerPath.isFile() ? "FILE: " : "DIR:  ";
            System.out.println(prefix + innerPath.getAbsolutePath());
        }

    }
}
