import com.study.io.WebServer.WebServer;

import java.io.IOException;

public class Test {
    public static void main(String[] args) throws IOException {
        WebServer ws = new WebServer();
        ws.setPort(123);
        ws.setWebAppPath("\\resources\\webapp\\");
        ws.start();
    }
}

