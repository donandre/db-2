package com.study.onlineshop.technical;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import static org.junit.Assert.*;

public class PropertyServiceTest {
    @Before
    public void createPropertyFile() throws IOException {
        String filePath = "src/main/resources/testProperty";

        File testProp = new File(filePath);
        testProp.createNewFile();
        String property1 = "P1_name:P1_val";
        String property2 = "P2_name:P2_val";
        FileOutputStream outputStream = new FileOutputStream(testProp);
        outputStream.write(property1.getBytes());
        outputStream.write(System.getProperty("line.separator").getBytes());
        outputStream.write(property2.getBytes());
        outputStream.close();
    }

    @Test
    public void getAllProperties() throws IOException {
        PropertyService ps = new PropertyService(new File("src/main/resources/testProperty"));
        List<Property> actual = ps.getAllProperties();
        assertNotNull(actual);
        Property p1 = new Property("P1_name", "P1_val");
        Property p2 = new Property("P2_name", "P2_val");
        List<Property> expected = new ArrayList<>();
        expected.add(p1);
        expected.add(p2);
        assertTrue(actual.containsAll(expected));

    }

    @Test
    public void getProperty() throws IOException {
        PropertyService ps = new PropertyService(new File("src/main/resources/testProperty"));
        Property actual = ps.getProperty("P2_name");
        assertNotNull(actual);
        assertEquals(actual.getName(), "P2_name");
        assertEquals(actual.getValue(), "P2_val");
    }

    @After
    public void removePropertyFile() {
        String filePath = "src/main/resources/testProperty";
        File testProp = new File(filePath);
        if (testProp.exists()) {
            testProp.delete();
        }
    }

}