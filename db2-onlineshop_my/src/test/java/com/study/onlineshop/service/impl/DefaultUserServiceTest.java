package com.study.onlineshop.service.impl;

import com.study.onlineshop.dao.UserDao;
import com.study.onlineshop.entity.User;
import com.study.onlineshop.security.SecurityUtils;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DefaultUserServiceTest {

    @Test
    public void getAll() {
        UserDao mockUserDao = mock(UserDao.class);
        User user1 = new User();
        user1.setId(1);
        User user2 = new User();
        user1.setId(2);
        List<User> expected = new ArrayList<>();
        expected.add(user1);
        expected.add(user2);
        when(mockUserDao.getAll()).thenReturn(expected);
        DefaultUserService defaultUserService = new DefaultUserService(mockUserDao);
        List<User> actual = defaultUserService.getAll();
        assertTrue(expected.containsAll(actual));
    }

    @Test
    public void getUser() {
        UserDao mockUserDao = mock(UserDao.class);
        User user1 = new User();
        user1.setId(1);
        user1.setLogin("user1_login");
        user1.setSole("user1_sole");
        user1.setPassword(SecurityUtils.getHash("user1_password", user1.getSole()));
        User user2 = new User();
        user2.setId(2);
        user2.setLogin("user2_login");
        user2.setSole("user2_sole");
        user2.setPassword(SecurityUtils.getHash("user1_password", user2.getSole()));
        when(mockUserDao.getUser("user1_login")).thenReturn(user1).thenReturn(null);
        DefaultUserService defaultUserService = new DefaultUserService(mockUserDao);
        User actual = defaultUserService.getUser("user1_login", "user1_password");
        assertTrue(user1.equals(actual));
        assertNull(defaultUserService.getUser("user1_login", "user1_password"));
        assertNull(defaultUserService.getUser("user3_login", "user3_password"));
        assertNull(defaultUserService.getUser("user1_login", "user3_password"));
    }
}