package com.study.onlineshop.service.impl;

import com.study.onlineshop.dao.ProductDao;
import com.study.onlineshop.entity.Product;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DefaultProductServiceTest {

    @Test
    public void getAll() {
        Product p1 = new Product();
        p1.setId(1);
        Product p2 = new Product();
        p2.setId(2);
        List<Product> expected = new ArrayList<>();
        expected.add(p1);
        expected.add(p2);
        ProductDao mockProductDao = mock(ProductDao.class);
        when(mockProductDao.getAll()).thenReturn(expected);
        DefaultProductService service = new DefaultProductService(mockProductDao);
        assertTrue(expected.containsAll(service.getAll()));
    }

    @Test
    public void getProducts() {
        Product p1 = new Product();
        p1.setId(1);
        List<Product> expected = new ArrayList<>();
        expected.add(p1);
        ProductDao mockProductDao = mock(ProductDao.class);
        when(mockProductDao.getProduct(1)).thenReturn(p1);
        DefaultProductService service = new DefaultProductService(mockProductDao);
        assertTrue(expected.containsAll(service.getProducts(1)));
        assertFalse(expected.containsAll(service.getProducts(2)));
    }

    @Test
    public void getProduct() {
        Product p1 = new Product();
        p1.setId(1);

        ProductDao mockProductDao = mock(ProductDao.class);
        when(mockProductDao.getProduct(1)).thenReturn(p1);
        DefaultProductService service = new DefaultProductService(mockProductDao);
        assertEquals(p1, service.getProduct(1));
        assertNotEquals(p1, service.getProduct(2));
    }

    @Test
    public void delete() {
        ProductDao mockProductDao = mock(ProductDao.class);
        DefaultProductService service = new DefaultProductService(mockProductDao);
        service.delete(5);
        verify(mockProductDao).delete(5);
    }

    @Test
    public void add() {
        ProductDao mockProductDao = mock(ProductDao.class);
        DefaultProductService service = new DefaultProductService(mockProductDao);
        LocalDateTime date = LocalDateTime.now();
        service.add("name", date, 1.0d);
        verify(mockProductDao).add("name", date, 1.0d);
    }

    @Test
    public void edit() {
        ProductDao mockProductDao = mock(ProductDao.class);
        DefaultProductService service = new DefaultProductService(mockProductDao);
        LocalDateTime date = LocalDateTime.now();
        service.edit(5, "name", date, 1.0d);
        verify(mockProductDao).edit(5, "name", date, 1.0d);
    }
}