package com.study.onlineshop.security;

import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.*;

public class SessionTest {

    @Test
    public void addProductToCart() {
        Session session = new Session();
        HashMap<Integer, Integer> actualCart = session.getCart();
        assertEquals(0, actualCart.size());
        session.addProductToCart(1);
        actualCart = session.getCart();
        assertEquals(1, actualCart.size());
        assertEquals(1, (int) actualCart.get(1));
        session.addProductToCart(1);
        actualCart = session.getCart();
        assertEquals(1, actualCart.size());
        assertEquals(2, (int) actualCart.get(1));
        session.addProductToCart(2);
        actualCart = session.getCart();
        assertEquals(2, actualCart.size());
        assertEquals(2, (int) actualCart.get(1));
        assertEquals(1, (int) actualCart.get(2));
        assertFalse(actualCart.containsKey(3));
    }

    @Test
    public void removeProductFromCart() {
        Session session = new Session();
        session.addProductToCart(1);
        session.addProductToCart(1);
        session.addProductToCart(2);
        HashMap<Integer, Integer> actualCart = session.getCart();
        assertEquals(2, actualCart.size());
        assertEquals(2, (int) actualCart.get(1));
        assertEquals(1, (int) actualCart.get(2));
        session.removeProductFromCart(1);
        actualCart = session.getCart();
        assertEquals(2, actualCart.size());
        assertEquals(1, (int) actualCart.get(1));
        assertEquals(1, (int) actualCart.get(2));
        session.removeProductFromCart(1);
        actualCart = session.getCart();
        assertFalse(actualCart.containsKey(1));
        assertEquals(1, actualCart.size());
        assertEquals(1, (int) actualCart.get(2));
        session.removeProductFromCart(2);
        actualCart = session.getCart();
        assertFalse(actualCart.containsKey(2));
        assertEquals(0, actualCart.size());
    }

    @Test
    public void removeAllProductsFromCart() {
        Session session = new Session();
        session.addProductToCart(1);
        session.addProductToCart(1);
        session.addProductToCart(2);
        HashMap<Integer, Integer> actualCart = session.getCart();
        assertEquals(2, actualCart.size());
        assertEquals(2, (int) actualCart.get(1));
        assertEquals(1, (int) actualCart.get(2));
        session.removeAllProductsFromCart();
        actualCart = session.getCart();
        assertEquals(0, actualCart.size());
        assertFalse(actualCart.containsKey(1));
        assertFalse(actualCart.containsKey(2));
    }
}