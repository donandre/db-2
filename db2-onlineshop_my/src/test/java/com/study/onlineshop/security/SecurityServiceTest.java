package com.study.onlineshop.security;

import com.study.onlineshop.entity.User;
import com.study.onlineshop.service.UserService;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SecurityServiceTest {

    @Test
    public void login() {
        UserService mockUserService = mock(UserService.class);
        User user = new User();
        user.setId(1);
        user.setLogin("user_name");
        when(mockUserService.getUser("user_name", "user_password")).thenReturn(user);
        SecurityService securityService = new SecurityService(mockUserService);
        Session newSession = securityService.login("user_name", "user_password");
        assertEquals(newSession.getUser(), user);
        assertEquals(newSession, securityService.getSession(newSession.getToken()));
    }

    @Test
    public void logout() {
        UserService mockUserService = mock(UserService.class);
        User user = new User();
        user.setId(1);
        user.setLogin("user_name");
        when(mockUserService.getUser("user_name", "user_password")).thenReturn(user);
        SecurityService securityService = new SecurityService(mockUserService);
        Session newSession = securityService.login("user_name", "user_password");
        assertEquals(newSession.getUser(), user);
        assertEquals(newSession, securityService.getSession(newSession.getToken()));
        securityService.logout(newSession.getToken());
        assertNull(securityService.getSession(newSession.getToken()));
    }

    @Test
    public void getSession() {
        UserService mockUserService = mock(UserService.class);
        User user = new User();
        user.setId(1);
        user.setLogin("user_name");
        when(mockUserService.getUser("user_name", "user_password")).thenReturn(user);
        SecurityService securityService = new SecurityService(mockUserService);
        Session newSession = securityService.login("user_name", "user_password");
        assertEquals(newSession.getUser(), user);
        assertEquals(newSession, securityService.getSession(newSession.getToken()));
        assertNull(securityService.getSession("invalid token"));
    }
}