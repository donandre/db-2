package com.study.onlineshop.security;

import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Test;

import static org.junit.Assert.*;

public class SecurityUtilsTest {

    @Test
    public void getHash() {
        String s1 = "name";
        String s2 = "password";
        String hash = DigestUtils.md5Hex(s1+s2).toUpperCase();
        assertEquals(hash, SecurityUtils.getHash(s1, s2));
    }
}