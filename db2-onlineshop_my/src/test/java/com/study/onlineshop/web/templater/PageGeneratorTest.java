package com.study.onlineshop.web.templater;

import org.junit.Test;

import java.io.File;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;


import static org.junit.Assert.*;

public class PageGeneratorTest {

    @Test
    public void instance() {

        assertNotNull(PageGenerator.instance());
    }

    @Test
    public void getPage() throws IOException {
        String filename = "testPage";
        String path = "templates/" + filename + "html";

        File testPage = new File(path);
        testPage.createNewFile();
        String expected = "Hello";
        FileOutputStream outputStream = new FileOutputStream(testPage);
        byte[] strToBytes = expected.getBytes();
        outputStream.write(strToBytes);
        outputStream.close();
        PageGenerator pg = new PageGenerator();
        String actual = pg.getPage(filename, new HashMap<String, Object>());
        assertEquals(expected, actual);
        testPage.delete();
    }
}