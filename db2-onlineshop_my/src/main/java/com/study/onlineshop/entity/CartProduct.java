package com.study.onlineshop.entity;

import java.time.LocalDateTime;

public class CartProduct extends Product {
    private int id;
    private String name;
    private LocalDateTime creationDate;
    private double price;
    private int quantity;

    public CartProduct(Product product, int quantity) {
        id=product.getId();
        name = product.getName();
        creationDate=product.getCreationDate();
        price = product.getPrice();
        this.quantity=quantity;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    @Override
    public double getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }
}
