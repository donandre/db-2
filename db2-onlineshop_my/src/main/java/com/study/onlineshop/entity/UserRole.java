package com.study.onlineshop.entity;

public enum UserRole {
    GUEST("GUEST"), USER("USER"), ADMIN("ADMIN");
    private String dbName;

    UserRole(String dbName) {
        this.dbName = dbName;
    }

    public static UserRole valueByDbName(String name) {
        UserRole[] allRoles = UserRole.values();
        for (UserRole role : allRoles) {
            if (role.dbName.equals(name)) {
                return role;
            }
        }
        return UserRole.GUEST;
    }
}
