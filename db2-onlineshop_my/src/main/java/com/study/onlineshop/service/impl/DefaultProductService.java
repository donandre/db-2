package com.study.onlineshop.service.impl;

import com.study.onlineshop.dao.ProductDao;
import com.study.onlineshop.entity.Product;
import com.study.onlineshop.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class DefaultProductService implements ProductService {
    @Autowired
    private ProductDao productDao;

    public DefaultProductService(ProductDao productDao) {
        this.productDao = productDao;
    }

    @Override
    public List<Product> getAll() {
        return productDao.getAll();
    }

    @Override
    public List<Product> getProducts(int id) {
        List <Product> ls = new ArrayList<>();
        ls.add(productDao.getProduct(id));
        return ls;
    }

    @Override
    public Product getProduct(int id) {
        return productDao.getProduct(id);
    }

    @Override
    public void delete(int id) {
        productDao.delete(id);
    }

    @Override
    public void add(String name, LocalDateTime creationDate, double price) {
        productDao.add(name, creationDate, price);
    }

    @Override
    public void edit(int id, String name, LocalDateTime creationDate, double price) {
        productDao.edit(id, name, creationDate, price);
    }
}
