package com.study.onlineshop.service;

import com.study.onlineshop.entity.Product;

import java.time.LocalDateTime;
import java.util.List;

public interface ProductService {
    List<Product> getAll();
    List<Product> getProducts(int id);
    Product getProduct(int id);
    void delete(int id);
    void add(String name, LocalDateTime creationDate, double price);
    void edit(int id, String name, LocalDateTime creationDate, double price);
}
