package com.study.onlineshop.technical;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class PropertyService {
    private List<Property> properties;
    private File propertyFile;

    public PropertyService() throws IOException {
        this.propertyFile = new File(new File("").getAbsolutePath() +
                "/src/main/resources/application.properties");
        properties = new ArrayList<>();
        fillProperties();
    }

    public PropertyService(File propertyFile) throws IOException {
        this.propertyFile = propertyFile;
        properties = new ArrayList<>();
        fillProperties();
    }

    private void fillProperties() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(propertyFile));
        String line;
        int delimeterPos;
        while ((line = reader.readLine()) != null) {
            delimeterPos = line.indexOf(":");
            if (delimeterPos > 0) {
                properties.add(new Property(line.substring(0, delimeterPos),
                        line.substring(delimeterPos + 1)));
            }

        }
        reader.close();
    }

    public List<Property> getAllProperties() throws IOException {
        return properties;
    }

    public Property getProperty(String PropertyName) throws IOException {
        for (Property property : properties
        ) {
            if (PropertyName.equals(property.getName())) {
                return property;
            }
        }
        return null;
    }
}
