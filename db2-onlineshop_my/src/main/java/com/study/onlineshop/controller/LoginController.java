package com.study.onlineshop.controller;

import com.study.onlineshop.security.SecurityService;
import com.study.onlineshop.security.Session;
import com.study.onlineshop.web.templater.PageGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

public class LoginController {


    private SecurityService securityService;


    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PageGenerator pageGenerator = PageGenerator.instance();
        HashMap<String, Object> parameters = new HashMap<>();
        String page = pageGenerator.getPage("login", parameters);
        resp.getWriter().write(page);
    }


    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        Session session = securityService.login(login, password);
        if (session != null) {
            String userToken = session.getToken();
            Cookie cookie = new Cookie("user-token", userToken);
            resp.addCookie(cookie);
            resp.sendRedirect("/products");
        } else {
            resp.sendRedirect("/login");
        }
    }
}
