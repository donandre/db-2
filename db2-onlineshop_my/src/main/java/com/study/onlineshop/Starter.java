package com.study.onlineshop;

import com.study.onlineshop.dao.jdbc.JdbcProductDao;
import com.study.onlineshop.dao.jdbc.JdbcUserDao;
import com.study.onlineshop.security.SecurityService;
import com.study.onlineshop.service.impl.DefaultProductService;
import com.study.onlineshop.service.impl.DefaultUserService;
import com.study.onlineshop.web.servlet.*;



public class Starter {
    public static void main(String[] args) throws Exception {
        // configure daos
        JdbcProductDao jdbcProductDao = new JdbcProductDao();
        JdbcUserDao jdbcUserDao = new JdbcUserDao();
        // configure services
        DefaultProductService defaultProductService = new DefaultProductService(jdbcProductDao);
        DefaultUserService defaultUserService = new DefaultUserService(jdbcUserDao);
        SecurityService securityService = new SecurityService(defaultUserService);
        // servlets
        LoginServlet loginServlet = new LoginServlet(securityService);
        LogoutServlet logoutServlet = new LogoutServlet(securityService);
        ProductsServlet productsServlet = new ProductsServlet(defaultProductService);
        AddProductServlet addProductServlet = new AddProductServlet(defaultProductService);
        EditProductServlet editProductServlet = new EditProductServlet(defaultProductService);
        DeleteProductServlet deleteProductServlet = new DeleteProductServlet(defaultProductService);
        AddToCartServlet addToCartServlet = new AddToCartServlet(securityService);
        CartServlet cartServlet = new CartServlet(defaultProductService, securityService);
        ChangeCartServlet changeCartServlet = new ChangeCartServlet(securityService);

        // config web server
        /*
        ServletContextHandler servletContextHandler = new ServletContextHandler();
        servletContextHandler.addFilter(new FilterHolder(new UserRoleSecurityFilter(securityService)),
                "/products", EnumSet.of(DispatcherType.REQUEST));
        servletContextHandler.addFilter(new FilterHolder(new UserRoleSecurityFilter(securityService)),
                "/product/*", EnumSet.of(DispatcherType.REQUEST));
        servletContextHandler.addServlet(new ServletHolder(productsServlet), "/products");

        servletContextHandler.addServlet(new ServletHolder(addProductServlet), "/product/add/");
        servletContextHandler.addServlet(new ServletHolder(editProductServlet), "/product/edit/");
        servletContextHandler.addServlet(new ServletHolder(deleteProductServlet), "/product/delete/");
        servletContextHandler.addServlet(new ServletHolder(cartServlet), "/product/cart/");
        servletContextHandler.addServlet(new ServletHolder(addToCartServlet), "/product/cart/add/");
        servletContextHandler.addServlet(new ServletHolder(changeCartServlet), "/product/cart/change/");

        AdminRoleSecurityFilter adminRoleSecurityFilter = new AdminRoleSecurityFilter(securityService);
        servletContextHandler.addFilter(new FilterHolder(adminRoleSecurityFilter),
                "/product/add/", EnumSet.of(DispatcherType.REQUEST));
        servletContextHandler.addFilter(new FilterHolder(adminRoleSecurityFilter),
                "/product/edit/", EnumSet.of(DispatcherType.REQUEST));
        servletContextHandler.addFilter(new FilterHolder(adminRoleSecurityFilter),
                "/product/delete/", EnumSet.of(DispatcherType.REQUEST));
        servletContextHandler.addServlet(new ServletHolder(loginServlet), "/");
        servletContextHandler.addServlet(new ServletHolder(logoutServlet), "/logout");
        PropertyService propertyService = new PropertyService();
        Server server = new Server(Integer.parseInt(propertyService.getProperty("server_port").getValue()));
        server.setHandler(servletContextHandler);
        server.start();
        */
    }
}
