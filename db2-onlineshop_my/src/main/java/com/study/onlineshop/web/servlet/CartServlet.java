package com.study.onlineshop.web.servlet;

import com.study.onlineshop.entity.CartProduct;
import com.study.onlineshop.entity.Product;
import com.study.onlineshop.security.SecurityService;
import com.study.onlineshop.security.Session;
import com.study.onlineshop.service.ProductService;
import com.study.onlineshop.web.templater.PageGenerator;


import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

public class CartServlet extends HttpServlet {
    private ProductService productService;
    private SecurityService securityService;

    public CartServlet(ProductService productService, SecurityService securityService) {
        this.productService = productService;
        this.securityService = securityService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Session session = null;
        List<CartProduct> cartProducts = new ArrayList<>();
        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("user-token")) {
                    session = securityService.getSession(cookie.getValue());
                    break;
                }
            }
        }
        if (session!=null) {
            HashMap<Integer, Integer> cart = session.getCart();
            if (cart.size() > 0) {
                Set entrySet = cart.entrySet();
                Iterator it = entrySet.iterator();
                while (it.hasNext()) {
                    Map.Entry me = (Map.Entry) it.next();
                    cartProducts.add(new CartProduct(productService.getProduct((int)me.getKey()), (int)me.getValue()));
                }
            }
        }
        PageGenerator pageGenerator = PageGenerator.instance();
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put("products", cartProducts);
        String page = pageGenerator.getPage("cart", parameters);
        resp.getWriter().write(page);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
    }

}
