package com.study.onlineshop.web.servlet;

import com.study.onlineshop.security.SecurityService;
import com.study.onlineshop.security.Session;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ChangeCartServlet extends HttpServlet {
    private SecurityService securityService;

    public ChangeCartServlet(SecurityService securityService) {
        this.securityService = securityService;
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String operation = req.getParameter("op");
        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("user-token")) {
                    String token = cookie.getValue();
                    Session session = securityService.getSession(token);
                    if (session != null) {
                        if (operation.equals("dec")) {
                            session.removeProductFromCart(Integer.parseInt(req.getParameter("id")));
                        } else if (operation.equals("inc")) {
                            session.addProductToCart(Integer.parseInt(req.getParameter("id")));
                        } else if (operation.equals("rm")) {
                            session.removeAllProductsFromCart();
                        }
                    }
                    break;
                }
            }
        }
        resp.sendRedirect("/product/cart/");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
    }
}