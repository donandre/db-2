package com.study.onlineshop.dao.file;

import com.study.onlineshop.dao.PropertyDao;
import com.study.onlineshop.technical.Property;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FilePropertyDao implements PropertyDao {

    private static List<Property> getAll() throws IOException {
        List<Property> properties = new ArrayList<>();
        File propertyFile = new File(new File("").getAbsolutePath() + "/src/main/resources/application.properties");
        List<String> records = new java.util.ArrayList<>();
        BufferedReader reader = new BufferedReader(new FileReader(propertyFile));
        String line;
        int delimeterPos;

        while ((line = reader.readLine()) != null) {
            delimeterPos = line.indexOf(":");
            if (delimeterPos > 0) {
                properties.add(new Property(line.substring(0, delimeterPos),
                        line.substring(delimeterPos + 1, line.length())));
            }

        }
        reader.close();
        return properties;
    }


    public static Property getProperty(String name) throws IOException {
        List<Property> properties = FilePropertyDao.getAll();
        for (Property property : properties
                ) {
            if (property.getName() == name) {
                return property;
            }
        }
        return null;
    }

    public static String getPropertyValue(String name) throws IOException {
        Property property = getProperty(name);
        if (property != null) {
            return getProperty(name).getValue();
        }
        return null;
    }
}
