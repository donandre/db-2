package com.study.onlineshop.dao.jdbc;

import com.study.onlineshop.technical.PropertyService;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class AbstractJdbcDao {
    protected Connection getConnection() throws SQLException, IOException {
        PropertyService ps = new PropertyService();
        ps.getProperty("database_username").getValue();
        String url = "jdbc:postgresql://" + ps.getProperty("database_hostname").getValue() +
                "/" + ps.getProperty("database_name").getValue();
        String name = ps.getProperty("database_username").getValue();
        String password = ps.getProperty("database_password").getValue();
        return DriverManager.getConnection(url, name, password);
    }
}
