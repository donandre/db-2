package com.study.onlineshop.dao;


import com.study.onlineshop.entity.User;

import java.time.LocalDateTime;
import java.util.List;

public interface UserDao {
    List<User> getAll();
    User getUser(String Name);
}
