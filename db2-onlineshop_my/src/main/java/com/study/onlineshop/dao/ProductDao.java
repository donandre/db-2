package com.study.onlineshop.dao;

import com.study.onlineshop.entity.Product;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;

public interface ProductDao {

    List<Product> getAll();
    void delete(int id);
    void add(String name, LocalDateTime creationDate, double price);
    void edit(int id, String name, LocalDateTime creationDate, double price);
    Product getProduct(int id);


}
