package com.study.onlineshop.security;

import com.study.onlineshop.entity.User;
import com.study.onlineshop.service.UserService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public class SecurityService {

    private List<Session> sessionList = new ArrayList<>();

    private UserService userService;

    public SecurityService(UserService userService) {
        this.userService = userService;
    }

    public Session login(String name, String password) {
        User user = userService.getUser(name, password);
        if (user != null) {
            Session session = new Session();
            session.setUser(user);
            session.setToken(UUID.randomUUID().toString());
            session.setExpireDate(LocalDateTime.now().plusHours(5));
            sessionList.add(session);
            return session;
        }
        return null;
    }

    public void logout(String token) {
        Iterator it = sessionList.iterator();
        while (it.hasNext()) {
            Session nextElem = (Session) it.next();
            if (nextElem.getExpireDate().isBefore(LocalDateTime.now())
                    || token.equals(nextElem.getToken())) {
                it.remove();
            }
        }
    }

    public Session getSession(String token) {
        Session session = null;
        Iterator it = sessionList.iterator();
        while (it.hasNext()) {
            Session nextElem = (Session) it.next();
            if (nextElem.getExpireDate().isBefore(LocalDateTime.now())) {
                it.remove();
                continue;
            }
            if (token.equals(nextElem.getToken())) {
                nextElem.setExpireDate(LocalDateTime.now().plusHours(5));
                session = nextElem;
            }
        }
        return session;
    }
}
