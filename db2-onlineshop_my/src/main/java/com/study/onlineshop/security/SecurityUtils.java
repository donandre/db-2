package com.study.onlineshop.security;

import org.apache.commons.codec.digest.DigestUtils;


public class SecurityUtils {
    public static String getHash(String pwd, String sole){
        return DigestUtils.md5Hex(pwd+sole).toUpperCase();
    }
}
