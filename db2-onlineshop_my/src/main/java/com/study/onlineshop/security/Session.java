package com.study.onlineshop.security;

import com.study.onlineshop.entity.User;

import java.time.LocalDateTime;
import java.util.*;

public class Session {
    private String token;
    private User user;
    private LocalDateTime expireDate;
    private HashMap<Integer, Integer> cart;

    public void addProductToCart(int id){
        if(cart == null){
            cart = new HashMap<>();
        }
        int count = cart.getOrDefault(id, 0);
        cart.put(id, count+1);
    }

    public void removeProductFromCart(int id){
        if(cart!=null && cart.containsKey(id)){
            int oldVal = cart.get(id);
            if (oldVal ==1){
                cart.remove(id);
            }
            else {
                cart.put(id, oldVal - 1);
            }
        }
    }

    public void removeAllProductsFromCart(){
        cart.clear();
    }

    public LocalDateTime getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(LocalDateTime expireDate) {
        this.expireDate = expireDate;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public HashMap<Integer, Integer> getCart() {
        if(cart == null){
            cart = new HashMap<>();
        }
        return cart;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Session)) return false;
        Session session = (Session) o;
        return Objects.equals(token, session.token);
    }
}
