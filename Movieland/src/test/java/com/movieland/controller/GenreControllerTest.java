package com.movieland.controller;

import com.movieland.entity.Genre;
import com.movieland.service.impl.GenreServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;


import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

public class GenreControllerTest {
    private List<Genre> output;
    @Mock
    GenreServiceImpl genreService;

    @InjectMocks
    GenreController genreController = new GenreController();

    private MockMvc mockMvc;


    @Before
    public void initialize(){
        output = new ArrayList<>();
        Genre genre1 = new Genre();
        Genre genre2 = new Genre();
        genre1.setName("name1");
        genre1.setId(1);
        genre2.setName("name2");
        genre2.setId(2);
        output.add(genre1);
        output.add(genre2);
        MockitoAnnotations.initMocks(this);
        mockMvc = standaloneSetup(genreController).build();
    }

    @Test
    public void getAllGenres() throws Exception {
        when(genreService.getAllGenres()).thenReturn(output);
        mockMvc.perform(get("/v1/genre")).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$", hasSize(2)))
        .andExpect(jsonPath("$[0].id", is(1)))
        .andExpect(jsonPath("$[0].name", is("name1")))
        .andExpect(jsonPath("$[1].id", is(2)))
        .andExpect(jsonPath("$[1].name", is("name2")));
        verify(genreService, times(1)).getAllGenres();
    }
}