package com.movieland.controller;

import com.movieland.entity.Movie;
import com.movieland.entity.OrderingParameter;
import com.movieland.service.impl.MovieServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;
import static org.junit.Assert.assertEquals;

public class MovieControllerTest {
    private List<Movie> output;
    private final int randomCount = 3;
    @Mock
    MovieServiceImpl movieService;

    @InjectMocks
    MovieController movieController = new MovieController();

    private MockMvc mockMvc;
    private Set<OrderingParameter> orderingParameterSet;


    @Before
    public void initialize() {
        output = new ArrayList<>();
        Movie movie1 = new Movie();
        Movie movie2 = new Movie();
        movie1.setNameNative("Movie 1");
        movie1.setId(1);
        movie1.setNameRussian("Фильм 1");
        movie2.setNameNative("Movie 2");
        movie2.setId(2);
        movie2.setNameRussian("Фильм 2");
        output.add(movie1);
        output.add(movie2);
        orderingParameterSet = new HashSet<>();
        OrderingParameter p1 = new OrderingParameter("price", "desc");
        orderingParameterSet.add(p1);
        movieController.setRandomCount(randomCount);
        MockitoAnnotations.initMocks(this);
        mockMvc = standaloneSetup(movieController).build();
    }

    @Test
    public void getAllMovies() throws Exception {
        when(movieService.getAllMovies(Collections.emptySet())).thenReturn(output);
        mockMvc.perform(get("/v1/movie"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].nameNative", is("Movie 1")))
                .andExpect(jsonPath("$[0].nameRussian", is("Фильм 1")))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].nameNative", is("Movie 2")))
                .andExpect(jsonPath("$[1].nameRussian", is("Фильм 2")));
        verify(movieService, times(1)).getAllMovies(Collections.emptySet());
    }

    @Test
    public void getRandomMovies() throws Exception {
        when(movieService.getRandomMovies(randomCount)).thenReturn(output);
        mockMvc.perform(get("/v1/movie/random"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].nameNative", is("Movie 1")))
                .andExpect(jsonPath("$[0].nameRussian", is("Фильм 1")))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].nameNative", is("Movie 2")))
                .andExpect(jsonPath("$[1].nameRussian", is("Фильм 2")));
        verify(movieService, times(1)).getRandomMovies(randomCount);
    }

    @Test
    public void getMoviesByGenre() throws Exception {
        int genreId = 666;
        int noMoviesGenreId = 13;
        when(movieService.getMoviesByGenre(genreId)).thenReturn(output);
        when(movieService.getMoviesByGenre(noMoviesGenreId)).thenReturn(null);
        mockMvc.perform(get("/v1/movie/genre/"+genreId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].nameNative", is("Movie 1")))
                .andExpect(jsonPath("$[0].nameRussian", is("Фильм 1")))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].nameNative", is("Movie 2")))
                .andExpect(jsonPath("$[1].nameRussian", is("Фильм 2")));
        verify(movieService, times(1)).getMoviesByGenre(genreId);
    }
    @Test
    public void getMoviesByGenreWithNoOutput() throws Exception {
        int noMoviesGenreId = 13;
        when(movieService.getMoviesByGenre(noMoviesGenreId)).thenReturn(new ArrayList<>());
        mockMvc.perform(get("/v1/movie/genre/"+noMoviesGenreId))
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));
        verify(movieService, times(1)).getMoviesByGenre(noMoviesGenreId);
    }

    @Test
    public void getOrderingParameters(){
        Map<String, String> input = new HashMap<>();
        Set<OrderingParameter> expected = new LinkedHashSet<>();
        input.put("param1","asc");
        OrderingParameter param1 = new OrderingParameter("param1", "asc");
        expected.add(param1);
        input.put("param2","asc");
        OrderingParameter param2 = new OrderingParameter("param2", "asc");
        expected.add(param2);
        input.put("param3","desc");
        OrderingParameter param3 = new OrderingParameter("param3", "desc");
        expected.add(param3);
        input.put("param4","value4");
        input.put("param5","");
        assertTrue(expected.containsAll(movieController.getOrderingParameters(input)));
        assertTrue(movieController.getOrderingParameters(input).containsAll(expected));
        assertEquals(3, movieController.getOrderingParameters(input).size());
    }


}