package com.movieland.dao.jdbc.mapper;

import com.movieland.entity.Movie;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MovieRowMapperTest {

    @Test
    public void mapRow() throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);
        String nameNative = "name_native";
        int yearOfRelease = 2000;
        int id = 1;
        String nameRussian = "name_rus";
        double rating = 1.0d;
        double price = 100.3d;
        String picturePath = "picture_path";
        Movie expected = new Movie();
        expected.setNameNative(nameNative);
        expected.setReleaseYear(yearOfRelease);
        expected.setNameRussian(nameRussian);
        expected.setId(id);
        expected.setPicturePath(picturePath);
        expected.setPrice(price);
        expected.setRating(rating);
        when(resultSet.getString("name_native")).thenReturn(nameNative);
        when(resultSet.getInt("year_of_release")).thenReturn(yearOfRelease);
        when(resultSet.getInt("id")).thenReturn(id);
        when(resultSet.getString("name_russian")).thenReturn(nameRussian);
        when(resultSet.getDouble("rating")).thenReturn(rating);
        when(resultSet.getDouble("price")).thenReturn(price);
        when(resultSet.getString("picture_path")).thenReturn(picturePath);
        MovieRowMapper movieRowMapper = new MovieRowMapper();
        Movie actual = movieRowMapper.mapRow(resultSet, 0);
        assertEquals(expected, actual);
    }
}