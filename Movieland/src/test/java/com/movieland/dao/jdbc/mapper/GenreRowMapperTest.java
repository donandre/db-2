package com.movieland.dao.jdbc.mapper;

import com.movieland.entity.Genre;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GenreRowMapperTest {

    @Test
    public void mapRow() throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);
        String name = "genre_name";
        int id = 1;
        Genre expected = new Genre();
        expected.setName(name);
        expected.setId(id);
        when(resultSet.getString("name")).thenReturn(name);
        when(resultSet.getInt("id")).thenReturn(id);
        GenreRowMapper genreRowMapper = new GenreRowMapper();
        Genre actual = genreRowMapper.mapRow(resultSet, 0);
        assertEquals(expected, actual);
    }
}