package com.movieland.dao.jdbc;

import com.movieland.dao.jdbc.mapper.GenreRowMapper;
import com.movieland.entity.Genre;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class JdbcGenreDaoTest {

    @Test
    public void getAllGenres() {
        List<Genre> output = new ArrayList<>();
        Genre genre1 = new Genre();
        Genre genre2 = new Genre();
        genre1.setName("name1");
        genre1.setId(1);
        genre2.setName("name2");
        genre2.setId(2);
        output.add(genre1);
        output.add(genre2);
        String getAllMoviesSQL = "genre_sql_query";
        JdbcGenreDao genreDao = new JdbcGenreDao();
        genreDao.setGetAllGenresSQL(getAllMoviesSQL);
        JdbcTemplate mockJdbcTemplate = mock(JdbcTemplate.class);
        when(mockJdbcTemplate.query(eq(getAllMoviesSQL), (GenreRowMapper) anyObject())).thenReturn(output);
        genreDao.setJdbcTemplate(mockJdbcTemplate);
        assertEquals(output, genreDao.getAllGenres());
    }
}