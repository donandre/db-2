package com.movieland.dao.jdbc;

import com.movieland.entity.Genre;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

public class JdbcCachedGenresTest {
    private List<Genre> output = new ArrayList<>();
    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Before
    public void initialize() {
        Genre genre1 = new Genre();
        Genre genre2 = new Genre();
        genre1.setName("name1");
        genre1.setId(1);
        genre2.setName("name2");
        genre2.setId(2);
        output.add(genre1);
        output.add(genre2);
    }

    @Test
    public void getAllGenresFromCache() {
        long cachePeriodMs = 1000000L;
        int timesToRun = 10;
        JdbcGenreDao mockJdbcGenreDao = mock(JdbcGenreDao.class);
        when(mockJdbcGenreDao.getAllGenres()).thenReturn(output);
        JdbcCachedGenres jdbcCachedGenres = new JdbcCachedGenres(cachePeriodMs);
        jdbcCachedGenres.setGenreDao(mockJdbcGenreDao);
        GetAllGenresEmulator getAllGenresEmulator1 = new GetAllGenresEmulator(jdbcCachedGenres, timesToRun);
        GetAllGenresEmulator getAllGenresEmulator2 = new GetAllGenresEmulator(jdbcCachedGenres, timesToRun);
        GetAllGenresEmulator getAllGenresEmulator3 = new GetAllGenresEmulator(jdbcCachedGenres, timesToRun);
        GetAllGenresEmulator getAllGenresEmulator4 = new GetAllGenresEmulator(jdbcCachedGenres, timesToRun);
        Thread t1 = new Thread(getAllGenresEmulator1);
        Thread t2 = new Thread(getAllGenresEmulator2);
        Thread t3 = new Thread(getAllGenresEmulator3);
        Thread t4 = new Thread(getAllGenresEmulator4);
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        while (getAllGenresEmulator1.isNotFinished() ||
                getAllGenresEmulator2.isNotFinished() ||
                getAllGenresEmulator3.isNotFinished() ||
                getAllGenresEmulator4.isNotFinished()) {
            logger.info("waiting for get all genres emulators finish their work.");
        }
        verify(mockJdbcGenreDao, times(1)).getAllGenres();
    }

    @Test
    public void getAllGenresWithCacheRenewal() {
        long cachePeriodMs = -1L;
        int timesToRun = 10;
        JdbcGenreDao mockJdbcGenreDao = mock(JdbcGenreDao.class);
        when(mockJdbcGenreDao.getAllGenres()).thenReturn(output);
        JdbcCachedGenres jdbcCachedGenres = new JdbcCachedGenres(cachePeriodMs);
        jdbcCachedGenres.setGenreDao(mockJdbcGenreDao);
        GetAllGenresEmulator getAllGenresEmulator1 = new GetAllGenresEmulator(jdbcCachedGenres, timesToRun);
        GetAllGenresEmulator getAllGenresEmulator2 = new GetAllGenresEmulator(jdbcCachedGenres, timesToRun);
        GetAllGenresEmulator getAllGenresEmulator3 = new GetAllGenresEmulator(jdbcCachedGenres, timesToRun);
        GetAllGenresEmulator getAllGenresEmulator4 = new GetAllGenresEmulator(jdbcCachedGenres, timesToRun);
        Thread t1 = new Thread(getAllGenresEmulator1);
        Thread t2 = new Thread(getAllGenresEmulator2);
        Thread t3 = new Thread(getAllGenresEmulator3);
        Thread t4 = new Thread(getAllGenresEmulator4);
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        while (getAllGenresEmulator1.isNotFinished() ||
                getAllGenresEmulator2.isNotFinished() ||
                getAllGenresEmulator3.isNotFinished() ||
                getAllGenresEmulator4.isNotFinished()) {
            logger.info("waiting for get all genres emulators finish their work.");
        }
        verify(mockJdbcGenreDao, times(40)).getAllGenres();
    }


    private class GetAllGenresEmulator implements Runnable {
        JdbcCachedGenres cachedGenres;
        int timesToRun;
        int timesHadRun;

        GetAllGenresEmulator(JdbcCachedGenres cachedGenres, int timesToRun) {
            this.cachedGenres = cachedGenres;
            this.timesToRun = timesToRun;

        }

        @Override
        public void run() {
            for (int i = 0; i < timesToRun; i++) {
                cachedGenres.getAllGenres();
                timesHadRun++;
            }

        }

        boolean isNotFinished() {
            return timesToRun != timesHadRun;
        }
    }
}