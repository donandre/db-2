package com.movieland.dao.jdbc;

import com.movieland.dao.jdbc.mapper.MovieRowMapper;
import com.movieland.entity.Movie;
import com.movieland.entity.OrderingParameter;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class JdbcMovieDaoTest {
    private List<Movie> output;
    private Set<OrderingParameter> orderingParameterSet;

    @Before
    public void initialize() {
        output = new ArrayList<>();
        Movie movie1 = new Movie();
        Movie movie2 = new Movie();
        movie1.setNameNative("name1");
        movie1.setReleaseYear(1);
        movie2.setNameNative("name2");
        movie2.setReleaseYear(2);
        output.add(movie1);
        output.add(movie2);
        orderingParameterSet = new HashSet<>();
        OrderingParameter p1 = new OrderingParameter("price", "desc");
        orderingParameterSet.add(p1);
    }

    @Test
    public void getAllMovies() {
        String getAllMoviesSQL = "sql_query";
        JdbcMovieDao movieDao = new JdbcMovieDao();
        movieDao.setGetAllMoviesSQL(getAllMoviesSQL);
        JdbcTemplate mockJdbcTemplate = mock(JdbcTemplate.class);
        when(mockJdbcTemplate.query(eq(getAllMoviesSQL), (MovieRowMapper) anyObject())).thenReturn(output);
        movieDao.setJdbcTemplate(mockJdbcTemplate);
        assertEquals(output, movieDao.getAllMovies(Collections.emptySet()));
    }

    @Test
    public void getRandomMovies() {
        String getRandomMoviesSQL = "sql_query";
        JdbcMovieDao movieDao = new JdbcMovieDao();
        movieDao.setGetRandomMoviesSQL(getRandomMoviesSQL);
        JdbcTemplate mockJdbcTemplate = mock(JdbcTemplate.class);
        when(mockJdbcTemplate.query(eq(getRandomMoviesSQL), (MovieRowMapper) anyObject(), anyInt())).thenReturn(output);
        movieDao.setJdbcTemplate(mockJdbcTemplate);
        assertEquals(output, movieDao.getRandomMovies(3));
    }

    @Test
    public void getMoviesByGenre() {
        String getMovieByGenreSQL = "movie_by_genre_sql";
        int genreId = 666;
        JdbcMovieDao movieDao = new JdbcMovieDao();
        movieDao.setGetMoviesByGenreSQL(getMovieByGenreSQL);
        JdbcTemplate mockJdbcTemplate = mock(JdbcTemplate.class);
        when(mockJdbcTemplate.query(eq(getMovieByGenreSQL), (MovieRowMapper) anyObject(), anyInt())).thenReturn(output);
        movieDao.setJdbcTemplate(mockJdbcTemplate);
        assertEquals(output, movieDao.getMovieByGenre(genreId));
    }

    @Test
    public void appendOrderingParameters$toQueryWithoutOrder() {
        JdbcMovieDao jdbcMovieDao = new JdbcMovieDao();
        String preparedSQL = "prepared sql";
        StringBuilder expected = new StringBuilder();
        expected.append(preparedSQL);
        if (!orderingParameterSet.isEmpty()) {
            expected.append(" ORDER BY ");
            Iterator<OrderingParameter> orderingParametersIterator = orderingParameterSet.iterator();
            OrderingParameter parameter = orderingParametersIterator.next();
            expected.append(parameter.getFieldName());
            expected.append(" ");
            expected.append(parameter.getOrderingDirection());
            while (orderingParametersIterator.hasNext()) {
                parameter = orderingParametersIterator.next();
                expected.append(",");
                expected.append(parameter.getFieldName());
                expected.append(" ");
                expected.append(parameter.getOrderingDirection());
            }
        }
        assertEquals(expected.toString(), jdbcMovieDao.appendOrderingParameters(preparedSQL, orderingParameterSet));
    }

    @Test
    public void appendOrderingParameters$NoParameters() {
        JdbcMovieDao jdbcMovieDao = new JdbcMovieDao();
        String preparedSQL = "prepared sql";
        assertEquals(preparedSQL, jdbcMovieDao.appendOrderingParameters(preparedSQL, Collections.emptySet()));
    }

    @Test
    public void appendOrderingParameters$toQueryWithOrder() {
        JdbcMovieDao jdbcMovieDao = new JdbcMovieDao();
        String orderByKeyWord = "ORDER BY";
        String preparedSQL = "prepared sql ORDER BY field1 asc, field2 desc";
        StringBuilder expected = new StringBuilder();
        expected.append(preparedSQL, 0, preparedSQL.lastIndexOf(orderByKeyWord));
        if (!orderingParameterSet.isEmpty()) {

            expected.append(orderByKeyWord);
            expected.append(" ");
            Iterator<OrderingParameter> orderingParametersIterator = orderingParameterSet.iterator();
            OrderingParameter parameter = orderingParametersIterator.next();
            expected.append(parameter.getFieldName());
            expected.append(" ");
            expected.append(parameter.getOrderingDirection());
            while (orderingParametersIterator.hasNext()) {
                parameter = orderingParametersIterator.next();
                expected.append(",");
                expected.append(parameter.getFieldName());
                expected.append(" ");
                expected.append(parameter.getOrderingDirection());
            }
        }
        expected.append(",");
        expected.append(preparedSQL, preparedSQL.lastIndexOf(orderByKeyWord) + orderByKeyWord.length(),
                preparedSQL.length());
        assertEquals(expected.toString(), jdbcMovieDao.appendOrderingParameters(preparedSQL, orderingParameterSet));
    }

}