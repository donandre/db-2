package com.movieland.service.impl;

import com.movieland.dao.MovieDao;
import com.movieland.entity.Movie;
import com.movieland.entity.OrderingParameter;
import org.junit.Before;
import org.junit.Test;


import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MovieServiceImplTest {
    private List<Movie> output;
    private Set<OrderingParameter> orderingParameterSet;

    @Before
    public void initialize() {
        output = new ArrayList<>();
        Movie movie1 = new Movie();
        movie1.setNameNative("name1");
        movie1.setReleaseYear(1);
        Movie movie2 = new Movie();
        movie2.setNameNative("name2");
        movie2.setReleaseYear(2);
        output.add(movie1);
        output.add(movie2);
        orderingParameterSet = new HashSet<>();
        OrderingParameter p1 = new OrderingParameter("price", "desc");
        orderingParameterSet.add(p1);

    }

    @Test
    public void getAllMovies() {
        MovieDao mockMovieDao = mock(MovieDao.class);
        when(mockMovieDao.getAllMovies(orderingParameterSet)).thenReturn(Collections.emptyList());
        MovieServiceImpl movielandService = new MovieServiceImpl();
        movielandService.setMovieDao(mockMovieDao);
        assertTrue(movielandService.getAllMovies(orderingParameterSet).isEmpty());
        verify(mockMovieDao).getAllMovies(orderingParameterSet);
        when(mockMovieDao.getAllMovies(orderingParameterSet)).thenReturn(output);
        assertEquals(output, movielandService.getAllMovies(orderingParameterSet));
    }

    @Test
    public void getRandomMovies() {
        MovieDao mockMovieDao = mock(MovieDao.class);
        MovieServiceImpl movielandService = new MovieServiceImpl();
        movielandService.setMovieDao(mockMovieDao);
        when(mockMovieDao.getRandomMovies(0)).thenReturn(Collections.emptyList());
        when(mockMovieDao.getRandomMovies(output.size())).thenReturn(output);
        assertTrue(movielandService.getRandomMovies(0).isEmpty());
        verify(mockMovieDao).getRandomMovies(0);
        assertEquals(output, movielandService.getRandomMovies(output.size()));
    }

    @Test
    public void getMoviesByGenre() {
        MovieDao mockMovieDao = mock(MovieDao.class);
        MovieServiceImpl movielandService = new MovieServiceImpl();
        movielandService.setMovieDao(mockMovieDao);
        int genreId = 666;
        when(mockMovieDao.getMovieByGenre(genreId)).thenReturn(output);
        assertEquals(output, movielandService.getMoviesByGenre(genreId));
        verify(mockMovieDao).getMovieByGenre(genreId);
    }
}