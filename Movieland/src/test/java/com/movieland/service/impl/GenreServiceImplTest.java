package com.movieland.service.impl;

import com.movieland.dao.GenreDao;
import com.movieland.entity.Genre;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

public class GenreServiceImplTest {
    List<Genre> output;
    @Before
    public void initialize(){
        output = new ArrayList<>();
       Genre genre1 = new Genre();
       genre1.setId(1);
       genre1.setName("name1");
       Genre genre2 = new Genre();
       genre2.setId(2);
       genre2.setName("name2");
       output.add(genre1);
       output.add(genre2);
    }
    @Test
    public void getAllGenres() {
        GenreDao mockGenreDao = mock(GenreDao.class);
        when(mockGenreDao.getAllGenres()).thenReturn(output);
        GenreServiceImpl genreServiceImpl = new GenreServiceImpl();
        genreServiceImpl.setGenreDao(mockGenreDao);
        assertEquals(output, genreServiceImpl.getAllGenres());
        verify(mockGenreDao).getAllGenres();
    }
}