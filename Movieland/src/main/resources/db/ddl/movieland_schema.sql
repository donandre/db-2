SET search_path = movieland;
CREATE TABLE country (
    id bigserial primary key,
    name varchar(160) NOT NULL
);
CREATE TABLE movie_country(
    id bigserial primary key,
    movie_id integer NOT NULL,
    country_id integer NOT NULL
);
CREATE TABLE movie
(
  id bigserial primary key,
  name_russian varchar (200),
  name_native varchar NOT NULL,
  year_of_release date NOT NULL,
  description varchar,
  rating numeric(2,1),
  price numeric(15,2),
  picture_path varchar,
  votes integer
);
CREATE TABLE movie_genre(
    id bigserial primary key,
    movie_id integer NOT NULL,
    genre_id integer NOT NULL
);
CREATE TABLE movie_country(
    id bigserial primary key,
    movie_id integer NOT NULL,
    country_id integer NOT NULL
);
CREATE TABLE rating(
    id bigserial primary key,
    movie_id integer NOT NULL,
    user_id integer NOT NULL,
    rating numeric(3,1) NOT NULL
);
CREATE TABLE review
(
  id bigserial primary key,
  movie_id integer NOT NULL,
  user_id integer NOT NULL,
  description varchar
);
CREATE TABLE ROLE
(
  id bigserial primary key,
  name varchar NOT NULL
);
CREATE TABLE USERS
(
  id bigserial primary key,
  login varchar NOT NULL,
  full_name varchar,
  role_id integer NOT NULL,
  sole varchar NOT NULL,
  password varchar NOT NULL
);
