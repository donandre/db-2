package com.movieland.data;

import java.util.ArrayList;
import java.util.List;

public enum SortDirection {
    ASCENDING("asc"),
    DESCENDING("desc");
    private final String directionCode;

    SortDirection(String direction) {
        this.directionCode = direction;
    }

    public String getDirectionCode() {
        return this.directionCode;
    }


    public static List<String> getDirectionCodes(){
        List<String> result = new ArrayList<>();
        for (SortDirection sortDirection : SortDirection.values()){
            result.add(sortDirection.directionCode);
        }

        return result;
    }
}
