package com.movieland.entity;

import java.util.Objects;

public class OrderingParameter {
    private String fieldName;
    private String orderingDirection;

    public OrderingParameter(String fieldName, String orderingDirection) {
        this.fieldName = fieldName;
        this.orderingDirection = orderingDirection;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getOrderingDirection() {
        return orderingDirection;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderingParameter that = (OrderingParameter) o;
        return fieldName.equals(that.fieldName) &&
                orderingDirection.equals(that.orderingDirection);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fieldName, orderingDirection);
    }
}
