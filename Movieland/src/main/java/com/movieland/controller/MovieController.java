package com.movieland.controller;

import com.movieland.data.SortDirection;
import com.movieland.entity.Movie;
import com.movieland.entity.OrderingParameter;
import com.movieland.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


@RestController
@RequestMapping(value="/v1/movie")
public class MovieController {

    private MovieService movieService;
    private int randomCount;

    @Autowired
    public void setMovieService(MovieService movieService) {
        this.movieService = movieService;
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Movie> getAllMovies(@RequestParam Map<String, String> allRequestParams) {
        return movieService.getAllMovies(getOrderingParameters(allRequestParams));
    }

    @RequestMapping(value = "/random", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Movie> getRandomMovies() {
        return movieService.getRandomMovies(randomCount);
    }

    @RequestMapping(value = "/genre/{id}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public List<Movie> getMoviesByGenre(@PathVariable int id) {
        List<Movie> movies = movieService.getMoviesByGenre(id);
        return movies;
    }

    @Value("${movie.randomCount:3}")
    public void setRandomCount(int randomCount) {
        this.randomCount = randomCount;
    }

    Set<OrderingParameter> getOrderingParameters (Map<String, String> parameters){
        Set<OrderingParameter> extractedParameters = new LinkedHashSet<>();
        for(Map.Entry<String, String> entry : parameters.entrySet()){
            if (SortDirection.getDirectionCodes().contains(entry.getValue())){
                extractedParameters.add(new OrderingParameter(entry.getKey(), entry.getValue()));
            }
        }
        return extractedParameters;
    }

}
