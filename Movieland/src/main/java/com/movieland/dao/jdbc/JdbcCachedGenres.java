package com.movieland.dao.jdbc;

import com.movieland.dao.GenreDao;
import com.movieland.entity.Genre;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import java.util.List;

@Primary
@Repository
public class JdbcCachedGenres implements GenreDao {

    private final Object refreshLock = new Object();
    private long expireCacheMs = 0L;
    private List<Genre> cachedInstance = null;
    private final long cachePeriodMs;
    private GenreDao genreDao;
    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    public List<Genre> getAllGenres() {
        if (cachedInstance == null || System.currentTimeMillis() > expireCacheMs) {
            synchronized (refreshLock) {
                if (cachedInstance == null || System.currentTimeMillis() > expireCacheMs) {
                    logger.info("starting retrieval from JdbcGenreDao");
                    List<Genre> genres = getAllGenresFromDao();
                    logger.info("finished retrieval from JdbcGenreDao. Got " + genres.size() + " items");
                    cachedInstance = genres;
                    expireCacheMs = System.currentTimeMillis() + cachePeriodMs;
                }
            }
        }
        return cachedInstance;
    }

    private List<Genre> getAllGenresFromDao() {
        return genreDao.getAllGenres();
    }

    @Autowired
    public JdbcCachedGenres(@Value("${dao.cache.genre:14400000}") long cachePeriodMs) {
        this.cachePeriodMs = cachePeriodMs;
    }


    @Autowired
    public void setGenreDao( GenreDao genreDao) {
        this.genreDao = genreDao;
    }

}
