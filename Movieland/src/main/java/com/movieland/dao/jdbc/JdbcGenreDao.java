package com.movieland.dao.jdbc;

import com.movieland.dao.GenreDao;
import com.movieland.dao.jdbc.mapper.GenreRowMapper;
import com.movieland.entity.Genre;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class JdbcGenreDao implements GenreDao {
    private JdbcTemplate jdbcTemplate;
    private String getAllGenresSQL;
    private static final GenreRowMapper genreRowMapper = new GenreRowMapper();

    @Override
    public List<Genre> getAllGenres() {
        return jdbcTemplate.query(getAllGenresSQL, genreRowMapper);
    }

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    @Autowired
    public void setGetAllGenresSQL(String getAllGenresSQL) {
        this.getAllGenresSQL = getAllGenresSQL;
    }
}
