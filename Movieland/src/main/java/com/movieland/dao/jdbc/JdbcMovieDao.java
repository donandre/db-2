package com.movieland.dao.jdbc;

import com.movieland.dao.MovieDao;
import com.movieland.dao.jdbc.mapper.MovieRowMapper;
import com.movieland.entity.Movie;
import com.movieland.entity.OrderingParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

@Repository
public class JdbcMovieDao implements MovieDao {

    private JdbcTemplate jdbcTemplate;
    private String getAllMoviesSQL;
    private String getRandomMoviesSQL;
    private String getMoviesByGenreSQL;
    private static final MovieRowMapper movieRowMapper = new MovieRowMapper();

    @Override
    public List<Movie> getAllMovies(Set<OrderingParameter> orderingParameterSet) {

        return jdbcTemplate.query(appendOrderingParameters(getAllMoviesSQL, orderingParameterSet), movieRowMapper);
    }

    @Override
    public List<Movie> getRandomMovies(int count) {
        return jdbcTemplate.query(getRandomMoviesSQL, movieRowMapper, count);
    }

    @Override
    public List<Movie> getMovieByGenre(int genreId) {
        return jdbcTemplate.query(getMoviesByGenreSQL, movieRowMapper, genreId);
    }

     String appendOrderingParameters(String query, Set<OrderingParameter> orderingParameterSet) {
        if (orderingParameterSet.size() == 0) {
            return query;
        }
        StringBuilder clause = new StringBuilder();
        String orderByClause = "ORDER BY";
        Iterator<OrderingParameter> orderingParameterIterator = orderingParameterSet.iterator();
        OrderingParameter entity = orderingParameterIterator.next();
        clause.append(entity.getFieldName())
                .append(" ")
                .append(entity.getOrderingDirection());
        while (orderingParameterIterator.hasNext()) {
            clause.append(", ");
            entity = orderingParameterIterator.next();
            clause.append(entity.getFieldName())
                    .append(" ")
                    .append(entity.getOrderingDirection());
        }
        if (query.toUpperCase().lastIndexOf(orderByClause) < 0) {
            clause.insert(0, " ");
            clause.insert(0, orderByClause);
            clause.insert(0, " ");
            clause.insert(0, query.trim());
        } else {
            clause.insert(0, " ");
            int beginIndex = query.lastIndexOf(orderByClause) + orderByClause.length();
            clause.insert(0, query.substring(0, beginIndex));
            clause.append(",");
            clause.append(query.substring(beginIndex));
        }
        return clause.toString();
    }

    @Autowired
    void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Autowired
    void setGetAllMoviesSQL(String getAllMoviesSQL) {
        this.getAllMoviesSQL = getAllMoviesSQL;
    }

    @Autowired
    public void setGetRandomMoviesSQL(String getRandomMoviesSQL) {
        this.getRandomMoviesSQL = getRandomMoviesSQL;
    }

    @Autowired
    public void setGetMoviesByGenreSQL(String getMoviesByGenreSQL) {
        this.getMoviesByGenreSQL = getMoviesByGenreSQL;
    }
}
