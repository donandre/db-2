package com.movieland.dao;

import com.movieland.entity.Movie;
import com.movieland.entity.OrderingParameter;

import java.util.List;
import java.util.Set;

public interface MovieDao {
    List<Movie> getAllMovies(Set<OrderingParameter> orderingParameterSet);
    List<Movie> getRandomMovies(int count);
    List<Movie> getMovieByGenre(int genreId);
}
