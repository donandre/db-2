package com.movieland.service;

import com.movieland.entity.Genre;

import java.util.List;

public interface GenreService {
    List<Genre> getAllGenres();
}
