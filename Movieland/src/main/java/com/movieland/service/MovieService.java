package com.movieland.service;

import com.movieland.entity.Movie;
import com.movieland.entity.OrderingParameter;

import java.util.List;
import java.util.Set;

public interface MovieService {
    List<Movie> getAllMovies(Set<OrderingParameter> orderingParameterSet);
    List<Movie> getRandomMovies(int count);
    List<Movie> getMoviesByGenre(int genreId);
}
