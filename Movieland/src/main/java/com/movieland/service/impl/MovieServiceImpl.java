package com.movieland.service.impl;

import com.movieland.dao.MovieDao;
import com.movieland.entity.Movie;
import com.movieland.entity.OrderingParameter;
import com.movieland.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class MovieServiceImpl implements MovieService {

    private MovieDao movieDao;

    @Override
    public List<Movie> getAllMovies(Set<OrderingParameter> orderingParameterSet) {
        return movieDao.getAllMovies(orderingParameterSet);
    }

    @Override
    public List<Movie> getRandomMovies(int count) {
        return movieDao.getRandomMovies(count);
    }

    @Override
    public List<Movie> getMoviesByGenre(int genreId) {
        return movieDao.getMovieByGenre(genreId);
    }

    @Autowired
    public void setMovieDao(MovieDao movieDao) {
        this.movieDao = movieDao;
    }


}
